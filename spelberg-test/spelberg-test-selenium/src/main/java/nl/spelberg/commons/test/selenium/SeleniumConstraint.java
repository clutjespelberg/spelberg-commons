package nl.spelberg.commons.test.selenium;

import org.openqa.selenium.WebElement;

public interface SeleniumConstraint {

    boolean matches(WebElement element);

    SeleniumConstraint and(SeleniumConstraint... seleniumConstraints);

    SeleniumConstraint or(SeleniumConstraint... seleniumConstraints);

}
