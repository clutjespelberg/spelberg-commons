package nl.spelberg.commons.test.selenium;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import nl.spelberg.commons.utils.collections.Lists;
import org.springframework.core.convert.converter.Converter;
import static org.junit.Assert.*;

/**
 * List containing {@link SeleniumElement} instances.
 */
public class SeleniumElementList implements Iterable<SeleniumElement> {

    private final List<SeleniumElement> seleniumElements = new ArrayList<SeleniumElement>();

    public void add(SeleniumElement seleniumElement) {
        seleniumElements.add(seleniumElement);
    }

    public void assertEmpty() {
        assertTrue(seleniumElements.isEmpty());
    }

    public SeleniumElement atIndex(int index) {
        if (index < 0) {
            throw new IllegalArgumentException("index < 0");
        }
        int size = seleniumElements.size();
        if (index >= size) {
            fail("expected index " + index + " too large, size: " + size + ", seleniumElements: " + seleniumElements);
        }
        return seleniumElements.get(index);
    }

    public SeleniumElement asSingleElement() {
        int size = seleniumElements.size();
        if (size == 0) {
            throw new IllegalArgumentException("SeleniumElementList is empty");
        } else if (size > 1) {
            throw new IllegalArgumentException("SeleniumElementList has more than 1 elements");
        }
        return seleniumElements.get(0);
    }

    public void assertTextEquals(String... expectedValues) {
        List<String> actual = Lists.convert(seleniumElements, new Converter<SeleniumElement, String>() {
            @Override
            public String convert(SeleniumElement seleniumElement) {
                return seleniumElement.getWebElement().getText();
            }
        });

        List<String> expected = Arrays.asList(expectedValues);

        assertEquals(expected, actual);
    }

    public void assertAttributeEquals(final String attribute, String... expectedValues) {

        List<String> actual = Lists.convert(seleniumElements, new Converter<SeleniumElement, String>() {
            @Override
            public String convert(SeleniumElement seleniumElement) {
                return seleniumElement.getWebElement().getAttribute(attribute);
            }
        });

        List<String> expected = Arrays.asList(expectedValues);

        assertEquals(expected, actual);
    }

    public void assertTextContains(String... expectedValues) {
        List<String> actual = Lists.convert(seleniumElements, new Converter<SeleniumElement, String>() {
            @Override
            public String convert(SeleniumElement seleniumElement) {
                return seleniumElement.getWebElement().getText();
            }
        });

        List<String> expected = Arrays.asList(expectedValues);

        assertContains(expected, actual);
    }

    /**
     * Zoek voor ieder element uit de lijst de descendants op a.h.v een css selector. Bijvoorbeeld alle
     * td's (kolommen) uit een lijst van tr's (rijen).
     *
     * @param cssSelector   bv td als deze seleniumelementlist uit tr elementen bestaat
     * @param expectedCount aantal verwachte descendants per element uit deze seleniumelementlist
     */
    public List<List<SeleniumElement>> descendantsByCssSelector(String cssSelector, int expectedCount, SeleniumConstraint... constraints) {
        List<List<SeleniumElement>> descendants = new ArrayList<List<SeleniumElement>>();

        for (SeleniumElement element : seleniumElements) {
            SeleniumElementList seleniumElementList = element.byCssSelector(cssSelector, expectedCount, constraints);
            descendants.add(seleniumElementList.seleniumElements);
        }

        return descendants;
    }

    private void assertContains(List<String> expected, List<String> actual) {
        int expectedSize = expected.size();
        int actualSize = actual.size();
        assertEquals("expected size(" + expectedSize + ") is not equal to actual size(" + actualSize + ")\n" +
                "expected: " + expected + "\n  actual: " + actual, expectedSize, actualSize);

        for (int i = 0; i < expectedSize; i++) {
            String expectedValue = expected.get(i);
            String actualValue = actual.get(i);
            if (!actualValue.contains(expectedValue)) {
                fail("expected to contain '" + expectedValue + "' but actual value is '" + actualValue +
                        "':\nexpected: " + expected + "\n  actual: " + actual);
            }
        }

        // success
    }

    @Override
    public Iterator<SeleniumElement> iterator() {
        return seleniumElements.iterator();
    }
}