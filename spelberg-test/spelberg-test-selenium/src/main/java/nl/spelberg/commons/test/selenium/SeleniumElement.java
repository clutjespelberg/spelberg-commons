package nl.spelberg.commons.test.selenium;

import org.junit.ComparisonFailure;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.junit.Assert.*;

/**
 * Wrapper voor Selenium WebElement om het gedrag beter te kunnen sturen.
 */
public class SeleniumElement extends SeleniumSearch {
    private static final Logger log = LoggerFactory.getLogger(SeleniumElement.class);

    private final WebElement webElement;

    public SeleniumElement(WebDriver webDriver, WebElement webElement) {
        super(webDriver, webElement);
        this.webElement = webElement;
    }

    public WebElement getWebElement() {
        return webElement;
    }

    public void enterText(String text) {

        // fix selenium weirdness...
        text = text.replace("(", Keys.chord(Keys.SHIFT, "9"));
        text = text.replace("&", Keys.chord(Keys.SHIFT, "7"));

        webElement.clear();
        webElement.sendKeys(text);
    }

    public void click() {
        webElement.click();
    }

    public void clickUsingMouse() {
        Actions actions = new Actions(webDriver);
        actions.moveToElement(webElement);
        actions.click().perform();
    }

    public Actions createActions() {
        return new Actions(webDriver);
    }

    public void doubleClick() {
        new Actions(webDriver).doubleClick(webElement).perform();
    }

    public String getText() {
        return webElement.getText();
    }

    public String getValue() {
        return webElement.getAttribute("value");
    }

    public boolean isDisplayed() {
        return webElement.isDisplayed();
    }

    public void assertVisible() {
        assertTrue(webElement.isDisplayed());
    }

    public void assertInvisible() {
        assertFalse(webElement.isDisplayed());
    }

    public void assertTextEquals(String text) {
        try {
            assertEquals(text, webElement.getText());
        } catch (ComparisonFailure e) {
            log.error("Comparison failure. HTML input tags should be checked with assertValueEquals(text). Did you mean assertValueEquals(text)?");
            throw e;
        }
    }

    public void assertValueEquals(String text) {
        assertEquals(text, webElement.getAttribute("value"));
    }

    public void assertIsEmptyList() {
        this.byTagName("li", 0);
    }

    public void assertHasChildrenOfClass(String className, int expectedNumberOfChildren) {
        this.byClass(className, expectedNumberOfChildren);
    }

    public void assertHasNoChildrenOfClass(String className) {
        this.byClass(className, 0);
    }

    public Select getSelect() {
        return new Select(webElement);
    }
}
