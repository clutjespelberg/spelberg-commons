package nl.spelberg.commons.test.selenium;

import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;
import nl.spelberg.commons.utils.file.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FirefoxDriverFactory implements RemoteWebDriverFactory {

    private static final Logger log = LoggerFactory.getLogger(FirefoxDriverFactory.class);
    private String downloadDir;
    private Proxy proxy;
    private final Map<String, String> profilePreferences = new TreeMap<String, String>();

    /**
     * Create new driver factory using the system temp directory as downloaddir.
     *
     * @see #FirefoxDriverFactory(String, org.openqa.selenium.Proxy)
     */
    public FirefoxDriverFactory() {
        this(FileUtils.getTempDir());
    }

    /**
     * Create new driver factory using a custom downloaddir.
     *
     * @param downloadDir The download directory
     * @see #FirefoxDriverFactory()
     */
    public FirefoxDriverFactory(String downloadDir) {
        this.downloadDir = downloadDir;
        this.proxy = null;
    }

    /**
     * Create new driver factory using a custom downloaddir.
     *
     * @see #FirefoxDriverFactory()
     */
    public FirefoxDriverFactory(Proxy proxy) {
        this(FileUtils.getTempDir(), proxy);
    }

    /**
     * Create new driver factory using a custom downloaddir.
     *
     * @param downloadDir The download directory
     * @see #FirefoxDriverFactory()
     */
    public FirefoxDriverFactory(String downloadDir, Proxy proxy) {
        this.downloadDir = downloadDir;
        this.proxy = proxy;
    }

    public void setDownloadDir(String downloadDir) {
        this.downloadDir = downloadDir;
    }

    public void setProxy(Proxy proxy) {
        this.proxy = proxy;
    }

    public void setProfilePreference(String key, String value) {
        this.profilePreferences.put(key, value);
    }


    @Override
    public FirefoxDriver createRemoteWebDriver() {

        DesiredCapabilities desiredCapabilities = DesiredCapabilities.firefox();
        if (proxy != null) {
            desiredCapabilities.setCapability(CapabilityType.PROXY, proxy);
        }

        FirefoxDriver firefoxDriver = new FirefoxDriver(getFirefoxBinary(), getFirefoxProfile(), desiredCapabilities);

        // Set screen resolution: 1280*768 is the same as the presentation screen
        firefoxDriver.manage().window().setPosition(new Point(0, 0));
        firefoxDriver.manage().window().setSize(new Dimension(1800, 1000));

        return firefoxDriver;
    }

    /**
     * Info op <strong><a href="http://kb.mozillazine.org/Firefox_:_FAQs_:_About:config_Entries">About:config entries</a></strong>
     */
    private FirefoxProfile getFirefoxProfile() {
        FirefoxProfile profile;
        try {
            profile = FirefoxProfile.fromJson("");
            // Info op: http://kb.mozillazine.org/Firefox_:_FAQs_:_About:config_Entries

            profile.setPreference("network.http.max-connections-per-server", 50);
            profile.setPreference("browser.cache.disk.enable", false);
            profile.setPreference("browser.cache.memory.enable", false);
            profile.setPreference("browser.cache.offline.enable", false);
            profile.setPreference("network.http.use-cache", false);
            profile.setPreference("plugins.hide_infobar_for_missing_plugin", true);

            // properties voor het automatisch opslaan van bestanden (selenium kan niet (goed?) overweg met een download dialog)
            profile.setPreference("browser.download.folderList", 2);
            profile.setPreference("browser.download.dir", downloadDir);

            /*
                browser.download.manager.showWhenStarting: Boolean:
                    True (default): Show Download Manager window when a download begins
                    False: Opposite of the above
                        Note: In Firefox, this can be changed via "Tools → Options → Main / Downloads → Show the Downloads window
                              when downloading a file" (Firefox 2 and above) or "Tools → Options → Downloads / Download Manager →
                              Show Download Manager when a download begins" (Firefox 1.5)
            */
            profile.setPreference("browser.download.manager.showWhenStarting", false);

            /*
            browser.download.manager.showAlertOnComplete: Boolean:
                True (default): popup window at bottom right corner of the screen will appear once all downloads are finished.
                False: the popup window will not appear.
            */
            profile.setPreference("browser.helperApps.neverAsk.saveToDisk", false);

            /*
                browser.helperApps.neverAsk.saveToDisk: String:
                    A comma-separated list of MIME types to save to disk without asking what to use to open the file. Default value is an empty string.
            */
            profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "text/xml");

            for (Map.Entry<String, String> entry : profilePreferences.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                log.info("Setting custom profile preference: " + key + "=" + value);
                profile.setPreference(key, value);
            }


            log.debug("firefox profile preferences gezet");

        } catch (IOException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
        return profile;
    }

    private FirefoxBinary getFirefoxBinary() {
        FirefoxBinary firefoxBinary = new FirefoxBinary();
        String display = System.getProperty(LazyWebDriver.IT_XVFB_DISPLAY);
        if (StringUtils.isNotBlank(display) && !"${it.xvfb.display}".equals(display)) {
            log.info("System property -D" + LazyWebDriver.IT_XVFB_DISPLAY + " is set to '" + display +
                     "', running browser using DISPLAY=\"" + display + "\"");
            firefoxBinary.setEnvironmentProperty("DISPLAY", display);
        } else {
            log.info("System property -D" + LazyWebDriver.IT_XVFB_DISPLAY + " is not set, running browser on local display");
        }
        return firefoxBinary;
    }

}
