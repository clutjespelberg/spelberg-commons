package nl.spelberg.commons.test.selenium;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Sleeper;
import org.openqa.selenium.support.ui.SystemClock;

/**
 * Copied from {@link org.openqa.selenium.support.ui.WebDriverWait} but using {@link SearchContext} instead of {@link org.openqa.selenium.WebElement}.
 */
public class SeleniumSearchContextWait extends FluentWait<SearchContext> {

    public final static long DEFAULT_SLEEP_TIMEOUT_MILLIS = 500;

    public SeleniumSearchContextWait(SearchContext searchContext, long timeOutInSeconds) {
        super(searchContext, new SystemClock(), Sleeper.SYSTEM_SLEEPER);
        withTimeout(timeOutInSeconds, TimeUnit.SECONDS);
        pollingEvery(DEFAULT_SLEEP_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS);
        ignoring(NotFoundException.class);
    }

}
