package nl.spelberg.commons.test.selenium;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation voor Selenium Tests:<br/>
 * <ul>
 *     <li>Annoteer de testclass met:<br/> <code>@RunWith(SeleniumTestRunner.class)</code></li>
 *     <li>Annoteer een veld van het type {@link SeleniumDriver} met deze annotatie:<br/>
 *     <code>@SeleniumTestDriver<br/>
 *     private SeleniumDriver seleniumDriver;</code></li>
 * </ul>
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface SeleniumTestDriver {

    Class<? extends RemoteWebDriverFactory> webDriverFactory() default FirefoxDriverFactory.class;

}
