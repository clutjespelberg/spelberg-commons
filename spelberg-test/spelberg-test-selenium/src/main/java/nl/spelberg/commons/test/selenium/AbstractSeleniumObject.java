package nl.spelberg.commons.test.selenium;

import org.springframework.util.Assert;

public abstract class AbstractSeleniumObject {

    protected final SeleniumDriver seleniumDriver;

    public AbstractSeleniumObject(SeleniumDriver seleniumDriver) {
        Assert.notNull(seleniumDriver, "seleniumDriver is null");
        this.seleniumDriver = seleniumDriver;
    }

}
