package nl.spelberg.commons.test.selenium;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.util.Assert;
import static org.junit.Assert.*;

public class SeleniumDriver {

    public static final int TIME_OUT_IN_SECONDS = 10;

    private final LazyWebDriver webDriver;
    private final String baseUrl;

    public final SeleniumSearch search;

    public SeleniumDriver(LazyWebDriver webDriver, String baseUrl) {
        Assert.notNull(webDriver, "webDriver is null");
        this.webDriver = webDriver;
        this.baseUrl = baseUrl;
        this.search = new SeleniumSearch(webDriver);
    }

    public void setSeleniumAjaxSpinnerChecker(SeleniumAjaxSpinnerChecker seleniumAjaxSpinnerChecker) {
        this.search.setSeleniumAjaxSpinnerChecker(seleniumAjaxSpinnerChecker);
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void open(String url) {
        webDriver.get(url);
    }

    public String getCurrentUrl() {
        return webDriver.getCurrentUrl();
    }

    public String getTitle() {
        return webDriver.getTitle();
    }

    public WebDriver.Navigation navigate() {
        return webDriver.navigate();
    }

    public WebDriver.TargetLocator switchTo() {
        return webDriver.switchTo();
    }

    public void switchToWindow(String windowNameOrHandle, final String expectedPageTitle) {
        switchToWindow(windowNameOrHandle, new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver webDriver) {
                return webDriver.getTitle().equals(expectedPageTitle);
            }
        });
    }

    public void switchToWindow(String windowNameOrHandle, final ExpectedCondition<Boolean> expectedCondition) {
        Wait<WebDriver> wait = new WebDriverWait(webDriver, TIME_OUT_IN_SECONDS);
        try {
            webDriver.switchTo().window(windowNameOrHandle);
            wait.until(expectedCondition);
        } catch (Exception e) {
            fail("Could not navigate to window with name/handle '" + windowNameOrHandle + "'.");
        }
    }

    public void closeWindow(String windowNameOrHandle) {
        webDriver.switchTo().window(windowNameOrHandle);
        webDriver.close();
    }

    public void assertPageTitle(String expectedPageTitle) {
        assertEquals(expectedPageTitle, webDriver.getTitle());
    }

    public void reset() {
        webDriver.navigate().refresh();
    }

    public void switchToFrame(String frameId) {
        webDriver.switchTo().frame(frameId);
    }

    public Object executeScript(String javascript, Object... args) {
        return webDriver.executeScript(javascript, args);
    }

    public Object executeAsyncScript(String javascript, Object... args) {
        return webDriver.executeAsyncScript(javascript, args);
    }

    public void waitForJQueryAjax() {
        String currentWindow = webDriver.getWindowHandle();
        waitForJQueryAjaxOnWindow(currentWindow);
    }

    public void waitForJQueryAjaxOnWindow(final String windowNameOrHandle) {
        Wait<WebDriver> wait = new WebDriverWait(webDriver, TIME_OUT_IN_SECONDS);

        try {
            wait.until(new ExpectedCondition<Boolean>() {
                @Override
                public Boolean apply(WebDriver webDriver) {
                    if (windowNameOrHandle.equals(webDriver.getWindowHandle()) && (webDriver instanceof JavascriptExecutor)) {
                        Object notActive = ((JavascriptExecutor) webDriver).executeScript("if (typeof jQuery == 'undefined') { return true; } else { return !jQuery.active; }");

                        if (notActive instanceof Boolean) {
                            return (Boolean) notActive;
                        }
                    }

                    return true;
                }
            });
        } catch (TimeoutException e) {
            throw new SeleniumITFailedError("TIMEOUT: still running jquery ajax requests");
        } catch (Exception e) {
            throw new SeleniumITFailedError("waitForJQueryAjax: " + e.getMessage(), e);
        }
    }

    /**
     * @return true als er 0 of 1 window geopend is, anders false
     */
    public boolean isPopupWindowClosed() {
        try {
            return (new WebDriverWait(webDriver, TIME_OUT_IN_SECONDS)).until(new ExpectedCondition<Boolean>() {
                @Override
                public Boolean apply(WebDriver d) {
                    return d.getWindowHandles().size() < 2;
                }
            });
        } catch (TimeoutException e) {
            return false;
        } catch (Exception e) {
            throw new SeleniumITFailedError("isPopupWindowClosed: " + e.getMessage(), e);
        }
    }
}
