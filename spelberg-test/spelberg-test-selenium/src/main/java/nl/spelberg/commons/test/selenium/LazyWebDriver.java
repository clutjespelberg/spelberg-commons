package nl.spelberg.commons.test.selenium;

import java.util.List;
import java.util.Set;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.HasInputDevices;
import org.openqa.selenium.interactions.Keyboard;
import org.openqa.selenium.interactions.Mouse;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

/**
 * WebDriver decorator that provides lazy starting of the decorated {@link RemoteWebDriver}.
 */
public class LazyWebDriver implements WebDriver, HasInputDevices, JavascriptExecutor {

    private static final Logger log = LoggerFactory.getLogger(LazyWebDriver.class);

    public static final String IT_XVFB_DISPLAY = "it.xvfb.display";

    private RemoteWebDriverFactory remoteWebDriverFactory;

    private RemoteWebDriver webDriver;

    /**
     * Constructor to use when the webdriverfactory is already known.
     */
    public LazyWebDriver(RemoteWebDriverFactory remoteWebDriverFactory) {
        Assert.notNull(remoteWebDriverFactory, "remoteWebDriverFactory is null");
        this.remoteWebDriverFactory = remoteWebDriverFactory;
    }

    /**
     * Constructor to use when the webdriverfactory is not known yet, set it using
     * {@link #setRemoteWebDriverFactory(RemoteWebDriverFactory)}.
     */
    public LazyWebDriver() {
    }

    /**
     * Set the webdriverfactory when the {@link #LazyWebDriver()} constuctor is used. This setter can be called only
     * once.
     */
    public void setRemoteWebDriverFactory(RemoteWebDriverFactory remoteWebDriverFactory) {
        Assert.notNull(remoteWebDriverFactory, "remoteWebDriverFactory is null");
        this.remoteWebDriverFactory = remoteWebDriverFactory;
    }

    @Override
    public void get(String url) {
        getWebDriver().get(url);
    }

    @Override
    public String getCurrentUrl() {
        return getWebDriver().getCurrentUrl();
    }

    @Override
    public String getTitle() {
        return getWebDriver().getTitle();
    }

    @Override
    public List<WebElement> findElements(By by) {
        return getWebDriver().findElements(by);
    }

    @Override
    public WebElement findElement(By by) {
        return getWebDriver().findElement(by);
    }

    @Override
    public String getPageSource() {
        return getWebDriver().getPageSource();
    }

    @Override
    public void close() {
        getWebDriver().close();
    }

    @Override
    public void quit() {
        getWebDriver().quit();
    }

    @Override
    public Set<String> getWindowHandles() {
        return getWebDriver().getWindowHandles();
    }

    @Override
    public String getWindowHandle() {
        return getWebDriver().getWindowHandle();
    }

    @Override
    public TargetLocator switchTo() {
        return getWebDriver().switchTo();
    }

    @Override
    public Navigation navigate() {
        return getWebDriver().navigate();
    }

    @Override
    public Options manage() {
        return getWebDriver().manage();
    }

    @Override
    public Keyboard getKeyboard() {
        return getWebDriver().getKeyboard();
    }

    @Override
    public Mouse getMouse() {
        return getWebDriver().getMouse();
    }

    @Override
    public Object executeScript(String s, Object... objects) {
        return getWebDriver().executeScript(s, objects);
    }

    @Override
    public Object executeAsyncScript(String s, Object... objects) {
        return getWebDriver().executeAsyncScript(s, objects);
    }

    private synchronized RemoteWebDriver getWebDriver() {
        if (webDriver == null) {
            log.info("STARTING NEW WEBDRIVER...");
            Assert.state(remoteWebDriverFactory != null, "remoteWebDriverFactory is null");
            //noinspection ConstantConditions
            webDriver = remoteWebDriverFactory.createRemoteWebDriver();
        }
        return webDriver;
    }

    public synchronized void quitLazy() {
        if (webDriver != null) {
            log.info("CLOSING ALL WEBDRIVER WINDOWS AND QUITTING...");
            webDriver.quit();
            webDriver = null;
        }
    }

}
