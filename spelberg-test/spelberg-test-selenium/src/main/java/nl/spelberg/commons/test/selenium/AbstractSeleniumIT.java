package nl.spelberg.commons.test.selenium;

import org.junit.runner.RunWith;

/**
 * Basis voor Selenium tests.
 *
 * De Selenium tests worden gerund met de
 * <a href="http://maven.apache.org/surefire/maven-failsafe-plugin/plugin-info.html">Maven Failsafe Plugin</a>. Om te zorgen dat
 * deze plugin de tests draait moet de naam van de test eindigen met <strong>IT</strong>. Bijvoorbeeld <code>MyObjectIT</code>.
 *
 * @see SeleniumAjaxSpinnerChecker
 */
@RunWith(SeleniumTestRunner.class)
public abstract class AbstractSeleniumIT {

    @SeleniumTestDriver
    protected SeleniumDriver seleniumDriver;

}
