package nl.spelberg.commons.test.selenium;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SeleniumITFailedError extends Error {

    private static final Logger log = LoggerFactory.getLogger(SeleniumITFailedError.class);

    private final List<WebElement> foundElements;

    public SeleniumITFailedError(String message) {
        this((List<WebElement>) null, message);
    }

    public SeleniumITFailedError(String message, Throwable cause) {
        this((List<WebElement>) null, message, cause);
    }

    public SeleniumITFailedError(WebElement foundElement, String message) {
        this(Arrays.asList(foundElement), message);
    }

    public SeleniumITFailedError(WebElement foundElement, String message, Throwable cause) {
        this(Arrays.asList(foundElement), message, cause);
    }

    public SeleniumITFailedError(List<WebElement> foundElements, String message) {
        super(message);
        this.foundElements = foundElements;
        log.error(getMessage(), this);
    }

    public SeleniumITFailedError(List<WebElement> foundElements, String message, Throwable cause) {
        super(message, cause);
        this.foundElements = foundElements;
        log.error(getMessage(), this);
    }

    public List<WebElement> getFoundElements() {
        return foundElements;
    }

    @Override
    public String getMessage() {
        try {
            return super.getMessage() + "\n" + asString(foundElements);
        } catch (Throwable t) {
            return super.getMessage();
        }
    }

    private static String asString(List<WebElement> webElements) {
        try {
            List<String> texts = new ArrayList<String>();
            for (WebElement webElement : webElements) {
                String tagName = webElement.getTagName();
                texts.add("\n    tag(name='" + tagName + ",text='" + webElement.getText() + "')");
            }
            return "Found " + webElements.size() + " WebElements: " + texts.toString();
        } catch (Exception e) {
            return "[error: " + e.getMessage() + "]";
        }
    }

}
