package nl.spelberg.commons.test.selenium;

import org.openqa.selenium.WebElement;
import org.springframework.util.Assert;

public class NotSeleniumConstraint extends AbstractSeleniumConstraint {

    private final SeleniumConstraint notConstraint;

    public NotSeleniumConstraint(SeleniumConstraint notConstraint) {
        Assert.notNull(notConstraint, "notConstraint is null");
        this.notConstraint = notConstraint;
    }

    @Override
    public boolean matchesThis(WebElement webElement) {
        return !notConstraint.matches(webElement);
    }
}
