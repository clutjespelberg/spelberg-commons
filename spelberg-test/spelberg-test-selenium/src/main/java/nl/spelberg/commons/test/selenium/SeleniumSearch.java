package nl.spelberg.commons.test.selenium;

import com.google.common.base.Function;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import nl.spelberg.commons.utils.collections.Arrays;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Wait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

public class SeleniumSearch {

    private static final Logger log = LoggerFactory.getLogger(SeleniumSearch.class);

    public static final SeleniumConstraint DISPLAYED_VISIBLE = new DisplayedSeleniumConstraint(true);
    public static final SeleniumConstraint DISPLAYED_INVISIBLE = new DisplayedSeleniumConstraint(false);

    private final long slowDownMillis;

    protected final WebDriver webDriver;
    private final SearchContext searchContext;
    private SeleniumAjaxSpinnerChecker seleniumAjaxSpinnerChecker;

    public SeleniumSearch(WebDriver webDriver) {
        this(webDriver, webDriver);
    }

    public SeleniumSearch(WebDriver webDriver, SearchContext searchContext) {
        Assert.notNull(webDriver, "webDriver is null");
        Assert.notNull(searchContext, "searchContext is null");
        this.webDriver = webDriver;
        this.searchContext = searchContext;
        this.slowDownMillis = Integer.parseInt(System.getProperty("it.slowdown.millis", "0"));
    }

    public void setSeleniumAjaxSpinnerChecker(SeleniumAjaxSpinnerChecker seleniumAjaxSpinnerChecker) {
        this.seleniumAjaxSpinnerChecker = seleniumAjaxSpinnerChecker;
    }

    public SeleniumElement byId(String elementId, SeleniumConstraint... constraints) {
        return waitForSeleniumElement(By.id(elementId), true, false, constraints);
    }

    /**
     * Zoekt selenium element alleen in de invisible elementen
     */
    public SeleniumElement byIdInvisible(String elementId, SeleniumConstraint... constraints) {
        return waitForSeleniumElement(By.id(elementId), false, true, Arrays.join(DISPLAYED_INVISIBLE, constraints));
    }

    /**
     * Controleert of een element niet voorkomt
     */
    public void byIdNotExisting(String elementId, SeleniumConstraint... constraints) {
        waitForSeleniumElements(By.id(elementId), true, false, 0, constraints);
    }

    // TODO deze weghalen, zoeken op invisible elementen is vanuit je test niet wenselijk
    @Deprecated
    public SeleniumElement byId(String elementId, boolean checkForDisplayedElements, boolean checkForHiddenElements,
            SeleniumConstraint... constraints) {
        return waitForSeleniumElement(By.id(elementId), checkForDisplayedElements, checkForHiddenElements, constraints);
    }

    public SeleniumElement byName(String elementName, SeleniumConstraint... constraints) {
        return waitForSeleniumElement(By.name(elementName), true, false, constraints);
    }

    // TODO deze weghalen, zoeken op invisible elementen is vanuit je test niet wenselijk
    @Deprecated
    public SeleniumElement byName(String elementName, boolean checkForDisplayedElements, boolean checkForHiddenElements,
            SeleniumConstraint... constraints) {
        return waitForSeleniumElement(By.name(elementName), checkForDisplayedElements, checkForHiddenElements, constraints);
    }

    public SeleniumElementList byName(String name, int expectedCount, SeleniumConstraint... constraints) {
        return waitForSeleniumElements(By.name(name), true, false, expectedCount, constraints);
    }

    public SeleniumElement byClass(String elementClass, SeleniumConstraint... constraints) {
        return waitForSeleniumElement(By.className(elementClass), true, false, constraints);
    }

    public SeleniumElementList byClass(String elementClass, int expectedCount, SeleniumConstraint... constraints) {
        return waitForSeleniumElements(By.className(elementClass), true, false, expectedCount, constraints);
    }

    public SeleniumElementList byClass(String elementClass, boolean checkForDisplayedElements, boolean checkForHiddenElements,
            int expectedCount, SeleniumConstraint... constraints) {
        return waitForSeleniumElements(By.className(elementClass), checkForDisplayedElements, checkForHiddenElements, expectedCount,
                constraints);
    }

    public SeleniumElement byTagName(String tagName, SeleniumConstraint... constraints) {
        return waitForSeleniumElement(By.tagName(tagName), true, false, constraints);
    }

    public SeleniumElementList byTagName(String tagName, int expectedCount, SeleniumConstraint... constraints) {
        return waitForSeleniumElements(By.tagName(tagName), true, false, expectedCount, constraints);
    }

    public SeleniumElement byPartialLinkText(String linkText, SeleniumConstraint... constraints) {
        return waitForSeleniumElement(By.partialLinkText(linkText), true, false, constraints);
    }

    public SeleniumElementList byPartialLinkText(String linkText, int expectedCount, SeleniumConstraint... constraints) {
        return waitForSeleniumElements(By.partialLinkText(linkText), true, false, expectedCount, constraints);
    }

    public SeleniumElement byPartialText(String tagName, final String text, SeleniumConstraint... constraints) {
        return byTagName(tagName, Arrays.join(constraints, DISPLAYED_VISIBLE, new PartialTextSeleniumConstraint(text)));
    }

    public SeleniumElementList byPartialText(String tagName, String text, int expectedCount, SeleniumConstraint... constraints) {
        return byTagName(tagName, expectedCount, Arrays.join(constraints, DISPLAYED_VISIBLE, new PartialTextSeleniumConstraint(text)));
    }

    public SeleniumElement byCssSelector(String cssSelector, SeleniumConstraint... constraints) {
        return waitForSeleniumElement(By.cssSelector(cssSelector), true, false, constraints);
    }

    public SeleniumElementList byCssSelector(String cssSelector, int expectedCount, SeleniumConstraint... constraints) {
        return waitForSeleniumElements(By.cssSelector(cssSelector), true, false, expectedCount, constraints);
    }

    /**
     * Deze alleen gebruiken als er echt geen andere optie is (id, name, tagname).
     *
     * @see #byId(String, SeleniumConstraint...)
     * @see #byName(String, SeleniumConstraint...)
     * @see #byTagName(String, SeleniumConstraint...)
     */
    @Deprecated
    public SeleniumElement byXPath(String xpath, SeleniumConstraint... constraints) {
        return waitForSeleniumElement(By.xpath(xpath), true, false, constraints);
    }

    /**
     * Deze alleen gebruiken als er echt geen andere optie is (id, name, tagname).
     *
     * @see #byId(String, SeleniumConstraint...)
     * @see #byName(String, int, SeleniumConstraint...)
     * @see #byTagName(String, int, SeleniumConstraint...)
     */
    @Deprecated
    public SeleniumElementList byXPath(String xpath, int expectedCount, SeleniumConstraint... constraints) {
        return waitForSeleniumElements(By.xpath(xpath), true, false, expectedCount, constraints);
    }

    private SeleniumElement waitForSeleniumElement(By by, boolean checkForDisplayedElements, boolean checkForHiddenElements,
            final SeleniumConstraint[] constraints) {
        return new SeleniumElement(webDriver,
                waitForWebElement(by, checkForDisplayedElements, checkForHiddenElements, constraints));
    }

    private SeleniumElementList waitForSeleniumElements(By by, boolean checkForDisplayedElements, boolean checkForHiddenElements,
            int expectedCount, final SeleniumConstraint[] constraints) {

        List<WebElement> webElements =
                waitForWebElements(by, checkForDisplayedElements, checkForHiddenElements, expectedCount, constraints);

        SeleniumElementList seleniumElements = new SeleniumElementList();
        for (WebElement webElement : webElements) {
            seleniumElements.add(new SeleniumElement(webDriver, webElement));
        }
        return seleniumElements;
    }

    /**
     * Wacht tot er precies 1 web element gevonden is.
     *
     * @param by Selenium zoekpatroon waarmee het element gevonden kan worden.
     * @return het gevonden element, zodat hier direct actie op kan worden ondernomen.
     */
    private WebElement waitForWebElement(By by, boolean checkForDisplayedElements, boolean checkForHiddenElements,
            final SeleniumConstraint[] constraints) {
        return waitForExpectedNrOfElements(by, 1, checkForDisplayedElements, checkForHiddenElements, constraints).get(0);
    }

    /**
     * Wacht tot er precies <code>expectedCount</code> web elementen gevonden zijn.
     *
     * @param by Selenium zoekpatroon waarmee het element gevonden kan worden.
     * @param expectedCount Het aantal verwachte elementen.
     * @return de gevonden elementen, zodat hier direct actie op kan worden ondernomen.
     */
    private List<WebElement> waitForWebElements(By by, boolean checkForDisplayedElements, boolean checkForHiddenElements,
            int expectedCount, final SeleniumConstraint[] constraints) {
        return waitForExpectedNrOfElements(by, expectedCount, checkForDisplayedElements, checkForHiddenElements, constraints);
    }

    /**
     * Selenium wacht niet altijd op javascript acties, waardoor de test kan vastlopen omdat een bepaalde actie nog niet
     * mogelijk is.
     * Deze method bouwt waits in totdat een bepaald element een gespecificeerd aantal keren voorkomt op de pagina.
     * De methode faalt als binnen de gestelde tijd niet exact het opgegeven aantal elementen dat voldoet aan het patroon wordt
     * gevonden.
     *
     * @param seleniumPattern Selenium zoekpatroon waarmee het element gevonden kan worden.
     * @param nrOfExpectedElements Aantal verwachte elementen dat voldoet aan het Selenium zoekpatroon.
     * @param checkForDisplayedElements Als deze true is dan worden elements met {@link org.openqa.selenium
     * .WebElement#isDisplayed()}
     * op true geteld.
     * @param checkForHiddenElements Als deze true is dan worden elements met {@link org.openqa.selenium
     * .WebElement#isDisplayed()}
     * op false geteld.
     * @return de gevonden elementen, zodat hier direct actie op kan worden ondernomen.
     */
    private List<WebElement> waitForExpectedNrOfElements(final By seleniumPattern, final int nrOfExpectedElements,
            final boolean checkForDisplayedElements, final boolean checkForHiddenElements, final SeleniumConstraint[] constraints) {
        try {

            slowDownBefore(seleniumPattern, nrOfExpectedElements, checkForDisplayedElements, checkForHiddenElements);

            return waitForExpectedNrOfElementsInternal(seleniumPattern, nrOfExpectedElements, checkForDisplayedElements,
                    checkForHiddenElements, constraints);
        } catch (StaleElementReferenceException e) {
            // de eerste keer dat deze exception optreedt proberen we het nog een keer
            // dit kan voorkomen als een element al op de pagina stond maar de pagina een refresh krijgt
            try {
                Thread.sleep(500);
            } catch (InterruptedException e1) {
                log.error("InterruptedException while handling: " + e.getMessage(), e);
                throw new RuntimeException(e1.getMessage(), e1);
            }
            return waitForExpectedNrOfElementsInternal(seleniumPattern, nrOfExpectedElements, checkForDisplayedElements,
                    checkForHiddenElements, constraints);
        } finally {

            slowDownAfter(seleniumPattern, nrOfExpectedElements, checkForDisplayedElements, checkForHiddenElements);

        }
    }

    private static final ThreadLocal<Boolean> inSpinnerChecker = new ThreadLocal<Boolean>();

    private void waitForNoAjaxSpinner() {
        if (seleniumAjaxSpinnerChecker != null && inSpinnerChecker.get() == null) {
            inSpinnerChecker.set(Boolean.TRUE);
            try {
                seleniumAjaxSpinnerChecker.waitForNoAjaxSpinner();
            } finally {
                inSpinnerChecker.remove();
            }
        }
    }

    private List<WebElement> waitForExpectedNrOfElementsInternal(final By seleniumPattern, final int nrOfExpectedElements,
            final boolean includeVisibleElements, final boolean includeInvisibleElements, final SeleniumConstraint[] constraints) {
        Assert.notNull(seleniumPattern, "seleniumPattern is null");
        waitForNoAjaxSpinner();
        final List<WebElement> foundElements = new ArrayList<WebElement>();

        //Wacht totdat het aantal verwachte elementen op de pagina staat, tot een maximum aantal seconden
        Wait<SearchContext> wait = new SeleniumSearchContextWait(searchContext, SeleniumDriver.TIME_OUT_IN_SECONDS);
        try {
            wait.until(new Function<SearchContext, Boolean>() {

                public Boolean apply(SearchContext searchContext) {
                    List<WebElement> elements = searchContext.findElements(seleniumPattern);
                    foundElements.clear();
                    for (WebElement element : elements) {
                        if (isMatchedElement(element, includeVisibleElements, includeInvisibleElements, constraints)) {
                            foundElements.add(element);
                        }
                    }
                    return foundElements.size() == nrOfExpectedElements;
                }

            });
        } catch (TimeoutException e) {
            throw new SeleniumITFailedError(foundElements,
                    "TIMEOUT: Expected to find " + nrOfExpectedElements + " elements but found " + foundElements.size() +
                    " matching pattern: '" + seleniumPattern + "', searchContext: " + searchContext);
        } catch (StaleElementReferenceException e) {
            // deze niet afhandelen, wordt indien nodig apart afgevangen.
            throw e;
        } catch (Exception e) {
            throw new SeleniumITFailedError(foundElements,
                    "Bij het wachten op pattern [" + seleniumPattern + "] in searchContext: " +
                    searchContext + " is iets fout gegaan: " + e.getMessage(), e);
        }

        return foundElements;
    }

    private boolean isMatchedElement(WebElement element, boolean includeVisibleElements, boolean includeInvisibleElements,
            SeleniumConstraint... constraints) {

        List<SeleniumConstraint> constraintList = new ArrayList<SeleniumConstraint>();

        // only check for visible/invisible when no constraints are added
        if (constraints.length == 0) {
            // this part is to support the legacy booleans
            if (includeVisibleElements && !includeInvisibleElements) {
                constraintList.add(DISPLAYED_VISIBLE);
            }
            if (!includeVisibleElements && includeInvisibleElements) {
                constraintList.add(DISPLAYED_INVISIBLE);
            }
        } else {
            Collections.addAll(constraintList, constraints);
        }

        for (SeleniumConstraint constraint : constraintList) {
            if (!constraint.matches(element)) {
                return false;
            }
        }
        return true;
    }

    private void slowDownBefore(By seleniumPattern, int nrOfExpectedElements, boolean checkForDisplayedElements,
            boolean checkForHiddenElements) {
        slowDown("BEFORE: " + seleniumPattern + ", expected " + nrOfExpectedElements + ", displayed: " + checkForDisplayedElements +
                 ", hidden: " +
                 checkForHiddenElements);
    }

    private void slowDownAfter(By seleniumPattern, int nrOfExpectedElements, boolean checkForDisplayedElements,
            boolean checkForHiddenElements) {
        slowDown("AFTER : " + seleniumPattern + ", expected " + nrOfExpectedElements + ", displayed: " + checkForDisplayedElements +
                 ", hidden: " +
                 checkForHiddenElements);
    }

    private void slowDown(String message) {
        if (slowDownMillis > 0) {
            log.warn("SLOWDOWN(" + slowDownMillis + "ms): " + message);
            try {
                Thread.sleep(slowDownMillis);
            } catch (InterruptedException e) {
                throw new RuntimeException(e.getMessage(), e);
            }
        }
    }

    public static SeleniumConstraint not(SeleniumConstraint notConstraint) {
        return new NotSeleniumConstraint(notConstraint);
    }

    private static class PartialTextSeleniumConstraint extends AbstractSeleniumConstraint {
        private final String text;

        public PartialTextSeleniumConstraint(String text) {
            Assert.notNull(text, "text is null");
            this.text = text;
        }

        @Override
        public boolean matchesThis(WebElement webElement) {
            return StringUtils.contains(webElement.getText(), text);
        }
    }

    private static class DisplayedSeleniumConstraint extends AbstractSeleniumConstraint {
        private final boolean expectedDisplayedValue;

        private DisplayedSeleniumConstraint(boolean expectedDisplayedValue) {
            this.expectedDisplayedValue = expectedDisplayedValue;
        }

        @Override
        public boolean matchesThis(WebElement element) {
            return element.isDisplayed() == expectedDisplayedValue;
        }
    }

}
