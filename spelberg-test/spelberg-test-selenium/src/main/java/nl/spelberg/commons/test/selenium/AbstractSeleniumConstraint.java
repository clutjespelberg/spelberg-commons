package nl.spelberg.commons.test.selenium;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import nl.spelberg.commons.utils.collections.Arrays;
import org.openqa.selenium.WebElement;
import org.springframework.util.Assert;

public abstract class AbstractSeleniumConstraint implements SeleniumConstraint {

    private final List<SeleniumConstraint> andConstraints;
    private final List<SeleniumConstraint> orConstraints;

    public AbstractSeleniumConstraint() {
        this.andConstraints = new ArrayList<SeleniumConstraint>();
        this.orConstraints = new ArrayList<SeleniumConstraint>();
    }

    private AbstractSeleniumConstraint(List<SeleniumConstraint> andConstraints, List<SeleniumConstraint> orConstraints) {
        Assert.notNull(andConstraints, "andConstraints is null");
        Assert.notNull(orConstraints, "orConstraints is null");
        this.andConstraints = andConstraints;
        this.orConstraints = orConstraints;
    }

    @Override
    public final boolean matches(WebElement webElement) {
        boolean result = matchesThis(webElement);
        if (result) {
            for (SeleniumConstraint andConstraint : andConstraints) {
                if (!andConstraint.matches(webElement)) {
                    result = false;
                    break;
                }
            }
        }
        if (!result) {
            for (SeleniumConstraint orConstraint : orConstraints) {
                if (orConstraint.matches(webElement)) {
                    return true;
                }
            }
        }
        return result;
    }

    public abstract boolean matchesThis(WebElement webElement);

    @Override
    public SeleniumConstraint and(SeleniumConstraint... andConstraints) {
        return new DelegatingSeleniumConstraint(this, Arrays.asList(andConstraints), Collections.<SeleniumConstraint>emptyList());
    }

    @Override
    public SeleniumConstraint or(SeleniumConstraint... orConstraints) {
        return new DelegatingSeleniumConstraint(this, Collections.<SeleniumConstraint>emptyList(), Arrays.asList(orConstraints));
    }

    private class DelegatingSeleniumConstraint extends AbstractSeleniumConstraint {
        private final SeleniumConstraint delegate;

        private DelegatingSeleniumConstraint(SeleniumConstraint delegate, List<SeleniumConstraint> andConstraints,
                List<SeleniumConstraint> orConstraints) {
            super(andConstraints, orConstraints);
            Assert.notNull(delegate, "delegate is null");
            this.delegate = delegate;
        }

        @Override
        public boolean matchesThis(WebElement webElement) {
            return delegate.matches(webElement);
        }
    }
}
