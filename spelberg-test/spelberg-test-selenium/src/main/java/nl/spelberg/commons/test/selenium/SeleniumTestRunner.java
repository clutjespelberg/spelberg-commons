package nl.spelberg.commons.test.selenium;

import java.lang.reflect.Field;
import nl.spelberg.commons.utils.test.ReflectionUtils;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunListener;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.InitializationError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

public class SeleniumTestRunner extends BlockJUnit4ClassRunner {

    private static final Logger log = LoggerFactory.getLogger(SeleniumTestRunner.class);

    public static final String IT_HTTP_PORT = "it.http.port";

    public static final String IT_HOSTNAME = "it.hostname";

    public static final String IT_CONTEXT = "it.context";

    private static final LazyWebDriver lazyWebDriver = new LazyWebDriver();

    public SeleniumTestRunner(Class<?> testClass) throws InitializationError {
        super(testClass);
    }

    @Override
    protected Object createTest() throws Exception {

        // JUnit mag zelf de test instance maken
        Object testInstance = super.createTest();

        //
        // init lazyWebDriver
        //
        Field testDriverField = ReflectionUtils.getPrivateField(testInstance, SeleniumTestDriver.class);
        SeleniumTestDriver seleniumTestDriverAnnotation = testDriverField.getAnnotation(SeleniumTestDriver.class);
        RemoteWebDriverFactory remoteWebDriverFactory = seleniumTestDriverAnnotation.webDriverFactory().newInstance();
        lazyWebDriver.setRemoteWebDriverFactory(remoteWebDriverFactory);

        // create driver
        SeleniumDriver seleniumDriver = new SeleniumDriver(lazyWebDriver, determineBaseUrl());

        //
        // configure Ajax spinner checker
        //
        if (testInstance instanceof SeleniumAjaxSpinnerChecker) {
            seleniumDriver.setSeleniumAjaxSpinnerChecker((SeleniumAjaxSpinnerChecker) testInstance);
        }

        // zorg dat de SeleniumDriver in de test gezet wordt
        ReflectionUtils.setPrivateField(testDriverField, testInstance, seleniumDriver);

        return testInstance;
    }

    @Override
    public void run(RunNotifier notifier) {
        notifier.addListener(new SeleniumRunListener(lazyWebDriver));
        super.run(notifier);
    }

    private String determineBaseUrl() {
        StringBuilder sb = new StringBuilder("http://");

        String hostname = System.getProperty(IT_HOSTNAME);
        if (hostname == null) {
            hostname = "localhost";
        }
        sb.append(hostname);

        String httpPort = System.getProperty(IT_HTTP_PORT);
        Assert.hasText(httpPort, "System property " + IT_HTTP_PORT + " is niet gezet");
        try {
            Integer.parseInt(httpPort);
        } catch (NumberFormatException e) {
            throw new AssertionError("System property " + IT_HTTP_PORT + " is geen getal: '" + httpPort + "'");
        }
        sb.append(":");
        sb.append(httpPort);

        String context = System.getProperty(IT_CONTEXT);
        if (context != null) {
            sb.append("/");
            sb.append(context);
        }

        return sb.toString();
    }

    public static class SeleniumRunListener extends RunListener {

        private final LazyWebDriver lazyWebDriver;

        public SeleniumRunListener(LazyWebDriver lazyWebDriver) {
            Assert.notNull(lazyWebDriver, "lazyWebDriver is null");
            this.lazyWebDriver = lazyWebDriver;
        }

        @Override
        public void testRunFinished(Result result) throws Exception {
            lazyWebDriver.quitLazy();
        }

        @Override
        public void testFailure(Failure failure) throws Exception {
            log.info("TEST FAILURE: closing WEBDRIVER to make sure other tests are not influenced...");
            lazyWebDriver.quitLazy();
        }
    }
}
