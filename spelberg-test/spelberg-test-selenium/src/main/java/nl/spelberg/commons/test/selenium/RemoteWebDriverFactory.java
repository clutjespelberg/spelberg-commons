package nl.spelberg.commons.test.selenium;

import org.openqa.selenium.remote.RemoteWebDriver;

public interface RemoteWebDriverFactory {

    RemoteWebDriver createRemoteWebDriver();

}
