package nl.spelberg.commons.test.selenium;

/**
 * Let your test class implement this interface to have the selenium test wait for the ajax spinner to disappear before continuing.
 */
public interface SeleniumAjaxSpinnerChecker {

    /**
     * This method should wait until ajax spinner is not visible. It should return immediately when the ajax spinner is not visible.
     */
    void waitForNoAjaxSpinner();

}
