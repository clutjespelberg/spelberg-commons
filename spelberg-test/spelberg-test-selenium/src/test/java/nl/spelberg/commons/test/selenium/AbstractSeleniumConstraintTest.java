package nl.spelberg.commons.test.selenium;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebElement;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.RETURNS_SMART_NULLS;
import static org.mockito.Mockito.mock;

public class AbstractSeleniumConstraintTest {

    private WebElement webElement;
    private AbstractSeleniumConstraint falseConstraint;
    private AbstractSeleniumConstraint trueConstraint;

    @Before
    public void setUp() throws Exception {
        webElement = mock(WebElement.class, RETURNS_SMART_NULLS);
        falseConstraint = new StaticConstraint(false);
        trueConstraint = new StaticConstraint(true);
    }

    @Test
    public void testMatches() throws Exception {
        // execute
        assertTrue(trueConstraint.matches(webElement));
        assertFalse(falseConstraint.matches(webElement));
    }

    @Test
    public void testAnd() throws Exception {
        // execute
        assertTrue(trueConstraint.and(trueConstraint, trueConstraint, trueConstraint).matches(webElement));
        assertFalse(trueConstraint.and(falseConstraint, trueConstraint, trueConstraint).matches(webElement));
        assertFalse(trueConstraint.and(trueConstraint, falseConstraint, trueConstraint).matches(webElement));
        assertFalse(trueConstraint.and(trueConstraint, trueConstraint, falseConstraint).matches(webElement));
        assertFalse(falseConstraint.and(trueConstraint, trueConstraint, trueConstraint).matches(webElement));

        assertTrue(trueConstraint.and(trueConstraint).and(trueConstraint).and(trueConstraint).matches(webElement));
        assertFalse(falseConstraint.and(trueConstraint).and(trueConstraint).and(trueConstraint).matches(webElement));
        assertFalse(trueConstraint.and(falseConstraint).and(trueConstraint).and(trueConstraint).matches(webElement));
        assertFalse(trueConstraint.and(trueConstraint).and(falseConstraint).and(trueConstraint).matches(webElement));
        assertFalse(trueConstraint.and(trueConstraint).and(trueConstraint).and(falseConstraint).matches(webElement));
    }

    @Test
    public void testOr() throws Exception {
        assertTrue(trueConstraint.or(falseConstraint, falseConstraint, falseConstraint).matches(webElement));
        assertTrue(trueConstraint.or(falseConstraint, trueConstraint, trueConstraint).matches(webElement));
        assertTrue(trueConstraint.or(trueConstraint, trueConstraint, falseConstraint).matches(webElement));
        assertTrue(falseConstraint.or(falseConstraint, falseConstraint, trueConstraint).matches(webElement));
        assertTrue(falseConstraint.or(falseConstraint, trueConstraint, falseConstraint).matches(webElement));
        assertTrue(falseConstraint.or(trueConstraint, falseConstraint, falseConstraint).matches(webElement));
        assertFalse(falseConstraint.or(falseConstraint, falseConstraint, falseConstraint).matches(webElement));

        assertTrue(trueConstraint.or(trueConstraint).or(trueConstraint).or(trueConstraint).matches(webElement));
        assertTrue(falseConstraint.or(trueConstraint).or(trueConstraint).or(trueConstraint).matches(webElement));
        assertTrue(trueConstraint.or(falseConstraint).or(trueConstraint).or(trueConstraint).matches(webElement));
        assertTrue(trueConstraint.or(trueConstraint).or(falseConstraint).or(trueConstraint).matches(webElement));
        assertTrue(trueConstraint.or(trueConstraint).or(trueConstraint).or(falseConstraint).matches(webElement));
        assertFalse(falseConstraint.or(falseConstraint).or(falseConstraint).or(falseConstraint).matches(webElement));

        assertFalse(falseConstraint.or(trueConstraint).and(falseConstraint).or(falseConstraint).matches(webElement));
    }

    private static class StaticConstraint extends AbstractSeleniumConstraint {

        private final boolean matchResult;

        private StaticConstraint(boolean matchResult) {
            this.matchResult = matchResult;
        }

        @Override
        public boolean matchesThis(WebElement webElement) {
            return matchResult;
        }
    }
}
