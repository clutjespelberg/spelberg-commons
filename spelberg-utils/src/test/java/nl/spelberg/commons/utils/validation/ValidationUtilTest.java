package nl.spelberg.commons.utils.validation;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import nl.spelberg.commons.utils.ui.AbstractJsonObject;
import org.junit.Test;

import static org.junit.Assert.*;

public class ValidationUtilTest {
    @Test
    public void testNotNull() throws Exception {

        ValidationUtil.notNull("not null value", "nonNullField");

        try {
            ValidationUtil.notNull(null, "testField");
            fail();
        } catch (JsonValidationException e) {
            assertEquals("testField is null", e.getMessage());
        }
    }

    @Test
    public void testNotBlank() throws Exception {

        ValidationUtil.notBlank("not blank value", "nonNullField");

        try {
            ValidationUtil.notBlank(null, "testField");
            fail();
        } catch (JsonValidationException e) {
            assertEquals("testField is null", e.getMessage());
        }

        try {
            ValidationUtil.notBlank("", "testField");
            fail();
        } catch (JsonValidationException e) {
            assertEquals("testField is blank", e.getMessage());
        }
        try {
            ValidationUtil.notBlank("    ", "testField");
            fail();
        } catch (JsonValidationException e) {
            assertEquals("testField is blank", e.getMessage());
        }
    }

    @Test
    public void testNotNullValidate() throws Exception {

        ValidationUtil.notNullValidate(new StringJsonObject("Has Value"), "nonNullField");

        try {
            ValidationUtil.notNullValidate((AbstractJsonObject) null, "testField");
            fail();
        } catch (JsonValidationException e) {
            assertEquals("testField is null", e.getMessage());
        }

        try {
            ValidationUtil.notNullValidate(new StringJsonObject(null), "testField");
            fail();
        } catch (JsonValidationException e) {
            assertEquals("testField - value is null", e.getMessage());
        }

    }

    @Test
    public void testNotNullValidateList() throws Exception {

        ValidationUtil.notNullValidate(Arrays.asList(new StringJsonObject("Has Value"), new StringJsonObject("Has Value2")), "nonNullList");

        try {
            ValidationUtil.notNullValidate((List<AbstractJsonObject>) null, "testList");
            fail();
        } catch (JsonValidationException e) {
            assertEquals("testList is null", e.getMessage());
        }

        try {
            ValidationUtil.notNullValidate(Arrays.asList(new StringJsonObject(null)), "testList");
            fail();
        } catch (JsonValidationException e) {
            assertEquals("testList[0] - value is null", e.getMessage());
        }

    }

    @Test
    public void testNotEmptyValidateList() throws Exception {

        ValidationUtil.notEmptyValidate(Arrays.asList(new StringJsonObject("Has Value"), new StringJsonObject("Has Value2")), "nonNullList");

        try {
            ValidationUtil.notEmptyValidate(null, "testList");
            fail();
        } catch (JsonValidationException e) {
            assertEquals("testList is null", e.getMessage());
        }

        try {
            ValidationUtil.notEmptyValidate(Collections.<AbstractJsonObject>emptyList(), "testList");
            fail();
        } catch (JsonValidationException e) {
            assertEquals("testList is empty", e.getMessage());
        }

        try {
            ValidationUtil.notEmptyValidate(Arrays.asList(new StringJsonObject(null)), "testList");
            fail();
        } catch (JsonValidationException e) {
            assertEquals("testList[0] - value is null", e.getMessage());
        }

    }

    @Test
    public void testValidateOrNull() throws Exception {

        ValidationUtil.nullOrValidate(null, "testField");
        ValidationUtil.nullOrValidate(new StringJsonObject("Has Value"), "nonNullField");

        try {
            ValidationUtil.nullOrValidate(new StringJsonObject(null), "testField");
            fail();
        } catch (JsonValidationException e) {
            assertEquals("testField - value is null", e.getMessage());
        }

    }

    @Test
    public void testHasText() {
        try {
            ValidationUtil.hasText(null, "nullField");
            fail();
        } catch (JsonValidationException e) {
            assertEquals("nullField is null", e.getMessage());
        }
        try {
            ValidationUtil.hasText("", "nullField");
            fail();
        } catch (JsonValidationException e) {
            assertEquals("nullField moet een waarde hebben", e.getMessage());
        }
        try {
            ValidationUtil.hasText(" ", "nullField");
            fail();
        } catch (JsonValidationException e) {
            assertEquals("nullField moet een waarde hebben", e.getMessage());
        }
        try {
            ValidationUtil.hasText("      ", "nullField");
            fail();
        } catch (JsonValidationException e) {
            assertEquals("nullField moet een waarde hebben", e.getMessage());
        }

        ValidationUtil.hasText("a", "nullField");
        ValidationUtil.hasText("abc", "nullField");
        ValidationUtil.hasText("a b", "nullField");
        ValidationUtil.hasText(" a", "nullField");
        ValidationUtil.hasText("a ", "nullField");
        ValidationUtil.hasText(" a ", "nullField");
        ValidationUtil.hasText(" a b c    d  ", "nullField");

    }

    @Test
    public void testValidateAllFields() {
        ValidationUtil.validateAllFields(new Object());
        ValidationUtil.validateAllFields("");
        ValidationUtil.validateAllFields(new StringJsonObject("value"));
        ValidationUtil.validateAllFields(new ComplexJsonObject("value", 42, new StringJsonObject("hi there")));

        try {
            ValidationUtil.validateAllFields(null);
            fail();
        } catch (IllegalArgumentException e) {
            assertEquals("o is null", e.getMessage());
        }
        try {
            ValidationUtil.validateAllFields(new StringJsonObject(null));
            fail();
        } catch (JsonValidationException e) {
            assertEquals("value is null", e.getMessage());
        }
        try {
            ValidationUtil.validateAllFields(new StringJsonObject(""));
            fail();
        } catch (JsonValidationException e) {
            assertEquals("value is blank", e.getMessage());
        }

        try {
            ValidationUtil.validateAllFields(new ComplexJsonObject(null, 42, new StringJsonObject("hi there")));
            fail();
        } catch (JsonValidationException e) {
            assertEquals("s is null", e.getMessage());
        }
        try {
            ValidationUtil.validateAllFields(new ComplexJsonObject("", 42, new StringJsonObject("hi there")));
            fail();
        } catch (JsonValidationException e) {
            assertEquals("s is blank", e.getMessage());
        }
        try {
            ValidationUtil.validateAllFields(new ComplexJsonObject("forty-two", null, new StringJsonObject("hi there")));
            fail();
        } catch (JsonValidationException e) {
            assertEquals("i is null", e.getMessage());
        }
        try {
            ValidationUtil.validateAllFields(new ComplexJsonObject("forty-two", 42, null));
            fail();
        } catch (JsonValidationException e) {
            assertEquals("stringJsonObject is null", e.getMessage());
        }
        try {
            ValidationUtil.validateAllFields(new ComplexJsonObject("forty-two", 42, new StringJsonObject("")));
            fail();
        } catch (JsonValidationException e) {
            assertEquals("stringJsonObject - value is blank", e.getMessage());
        }
    }

    private static class StringJsonObject extends AbstractJsonObject {
        private String value;

        private StringJsonObject(String value) {
            this.value = value;
        }

        @Override
        public void jsonValidate() throws JsonValidationException {
            ValidationUtil.notBlank(value, "value");
        }
    }

    private static class ComplexJsonObject extends AbstractJsonObject {

        private String s;

        private Integer i;

        private StringJsonObject stringJsonObject;

        private ComplexJsonObject(String s, Integer i, StringJsonObject stringJsonObject) {
            this.s = s;
            this.i = i;
            this.stringJsonObject = stringJsonObject;
        }
    }

}
