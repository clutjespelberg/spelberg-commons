package nl.spelberg.commons.utils.validation;

import nl.spelberg.commons.utils.ui.UIException;
import org.junit.Test;

import static org.junit.Assert.*;

public class JsonValidationExceptionTest {

    @Test
    public void testConstructors() {
        assertValidationException("Onverwachte fout", null, new JsonValidationException((String) null));
        assertValidationException("Onverwachte fout", "User Mess", new JsonValidationException("User Mess"));
        assertValidationException("Onverwachte fout", "User Mess-up", new JsonValidationException(null, "User Mess-up"));
        assertValidationException("Custom Title", "User Mess-up", new JsonValidationException("Custom Title", "User Mess-up"));
        assertValidationException("Onverwachte fout", "User Mess", new JsonValidationException("User Mess", (JsonValidationException) null));
        assertValidationException("Onverwachte fout", "User Mess - Deeper Mess", new JsonValidationException("User Mess", new JsonValidationException("Deeper Mess")));
    }

    private void assertValidationException(String expectedTitle, String expectedMessage, JsonValidationException jsonValidationException) {
        assertEquals(UIException.Severity.WARNING, jsonValidationException.getSeverity());
        assertEquals(expectedTitle, jsonValidationException.getTitle());
        assertEquals(expectedMessage, jsonValidationException.getMessage());
    }

}
