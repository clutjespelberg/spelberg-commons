package nl.spelberg.commons.utils.collections;

import java.util.Arrays;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.springframework.core.convert.converter.Converter;

import static org.junit.Assert.*;

public class MapsTest {

    @Test
    public void testToMap() {
        assertEquals(Maps.map("key", "key:value"), Maps.asMap(Arrays.asList("key:value"), new Converter<String, String>() {
            @Override
            public String convert(String source) {
                return StringUtils.substringBefore(source, ":");
            }
        }));
    }

    @Test
    public void testFormatCustom() {
        assertEquals("<2>-'twee'\n<4>-'vier'",
                Maps.formatMap(Maps.sortedMap(Maps.entry(Integer.valueOf(4), "vier"), Maps.entry(Integer.valueOf(2), "twee")),
                        new Converter<Map.Entry<Integer, String>, String>() {

                            @Override
                            public String convert(Map.Entry<Integer, String> source) {
                                return "<" + source.getKey() + ">-'" + source.getValue() + "'";
                            }
                        }));
    }
}

