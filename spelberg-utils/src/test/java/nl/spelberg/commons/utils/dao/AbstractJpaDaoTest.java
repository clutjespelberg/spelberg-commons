package nl.spelberg.commons.utils.dao;

import java.util.Arrays;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.Id;
import javax.persistence.TypedQuery;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@SuppressWarnings("JpaQlInspection")
@RunWith(MockitoJUnitRunner.class)
public class AbstractJpaDaoTest {

    @Mock
    EntityManager entityManager;

    @InjectMocks
    private AbstractJpaDao<TestEntity, String> abstractJpaDao = new AbstractJpaDao<TestEntity, String>() {
    };

    @After
    public void tearDown() {
        verifyNoMoreInteractions(entityManager);
    }

    @Test
    public void testFindAll() {
        // setup
        @SuppressWarnings("unchecked")
        TypedQuery<TestEntity> query = mock(TypedQuery.class);
        when(query.getResultList()).thenReturn(Arrays.asList(new TestEntity("newId")));
        when(entityManager.createQuery("SELECT o FROM TestEntity o", TestEntity.class)).thenReturn(query);

        // execute
        List<TestEntity> all = abstractJpaDao.findAll();

        // verify
        verify(entityManager).createQuery("SELECT o FROM TestEntity o", TestEntity.class);
        List<TestEntity> expectedList = Arrays.asList(new TestEntity("newId"));
        assertEquals(expectedList, all);
    }

    @Test
    public void testPersist() {
        // setup
        TestEntity testEntity = new TestEntity();

        // execute
        TestEntity persistedEntity = abstractJpaDao.persist(testEntity);

        // verify
        InOrder inOrder = inOrder(entityManager);
        inOrder.verify(entityManager).persist(persistedEntity);
        inOrder.verify(entityManager).flush();
        assertEquals(persistedEntity, testEntity);
    }

    @Test
    public void testDelete() {
        // setup
        TestEntity testEntity = new TestEntity();

        // execute
        abstractJpaDao.delete(testEntity);

        // verify
        InOrder inOrder = inOrder(entityManager);
        inOrder.verify(entityManager).remove(testEntity);
        inOrder.verify(entityManager).flush();
    }

    @Test
    public void testDeleteById() {
        // setup
        TestEntity testEntity = new TestEntity("existingId");
        when(entityManager.find(TestEntity.class, testEntity.getId())).thenReturn(testEntity);

        // execute
        abstractJpaDao.deleteById(testEntity.getId());

        // verify
        verify(entityManager).find(TestEntity.class, testEntity.getId());
        InOrder inOrder = inOrder(entityManager);
        inOrder.verify(entityManager).remove(testEntity);
        inOrder.verify(entityManager).flush();
    }

    @Entity
    private class TestEntity extends AbstractQueryEntity {

        @Id
        private String id;

        private TestEntity() {
        }

        private TestEntity(String id) {
            this.id = id;
        }

        public String getId() {
            return id;
        }
    }
}
