package nl.spelberg.commons.utils.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import static org.junit.Assert.*;

public class AbstractUploadServletTest {

    private AbstractUploadServlet servlet;
    private MockHttpServletRequest request;
    private MockHttpServletResponse response;
    private RequestParameters expectedRequestParameters;
    private RequestParameters expectedRequestParameters1;
    private RequestParameters expectedRequestParameters2;

    @Before
    public void setUp() {
        expectedRequestParameters1 = new RequestParameters("param1", "value1");
        expectedRequestParameters2 = new RequestParameters("param2", "value2").add("param3", "value3");
        expectedRequestParameters = expectedRequestParameters1.join(expectedRequestParameters2);
        servlet = new TestServlet(expectedRequestParameters);
        request = new MockHttpServletRequest("POST", "http://localhost/");
        response = new MockHttpServletResponse();
    }

    @Test
    public void testUploadFile() throws ServletException, IOException {

        // setup
        String fileContent = "the filecontents";

        fillRequestWithFileContent(request, fileContent);

        // execute
        servlet.doPost(request, response);

        // verify
        assertEquals("the filecontents", response.getContentAsString());
    }

    private void fillRequestWithFileContent(MockHttpServletRequest request, String fileContent) throws IOException {
        String boundary = "ABCD----1234";
        String newLine = "\r\n";

        StringBuilder contentBuilder = new StringBuilder();
        contentBuilder.append(expectedRequestParameters1.asHttpPostData(boundary, newLine));
        contentBuilder.append("--" + boundary + newLine);
        contentBuilder.append("Content-Disposition: form-data; name=\"textField\"; filename=\"test.txt\"" + newLine);
        contentBuilder.append("Content-Type: text/plain" + newLine);
        contentBuilder.append(newLine);
        contentBuilder.append(fileContent + newLine);
        contentBuilder.append(expectedRequestParameters2.asHttpPostData(boundary, newLine));
        contentBuilder.append("--" + boundary + "--");
        byte[] content = contentBuilder.toString().getBytes();

        request.setContentType("multipart/form-data; boundary=" + boundary);
        request.setContent(content);
    }

    private static class TestServlet extends AbstractUploadServlet {

        private final RequestParameters expectedRequestParameters;

        private TestServlet(RequestParameters expectedRequestParameters) {
            this.expectedRequestParameters = expectedRequestParameters;
        }

        @Override
        public void processInputStream(InputStream input, RequestParameters requestParameters, HttpServletResponse response) {
            assertEquals("RequestParameters zijn niet zoals verwacht.", expectedRequestParameters, requestParameters);

            response.setContentType("text/html");
            response.setHeader("Pragma", "No-cache");
            response.setDateHeader("Expires", 0);
            response.setHeader("Cache-Control", "no-cache");
            PrintWriter out;
            try {
                BufferedReader in = new BufferedReader(new InputStreamReader(input));
                out = response.getWriter();
                String line;
                while ((line = in.readLine()) != null) {
                    out.print(line);
                }
                out.flush();
            } catch (IOException e) {
                throw new RuntimeException(e.getMessage(), e);
            }
        }
    }

}
