package nl.spelberg.commons.utils.collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.junit.Test;
import org.springframework.core.convert.converter.Converter;

import static org.junit.Assert.*;

public class SetsTest {
    @Test
    public void testAsSet() {
        assertEquals(Collections.emptySet(), Sets.asSet());
        assertEquals(new HashSet<String>(), Sets.asSet());
        assertEquals(Collections.singleton("Hi"), Sets.asSet("Hi"));
        assertEquals(new HashSet<String>(Arrays.asList("Hi", "There")), Sets.asSet("Hi", "There"));

        String[] values = {"Hi", "There", "This", "Is", "Still", "In", "The", "Same", "Order"};
        assertArrayEquals(values, Sets.asSet(values).toArray());

        assertEquals(Collections.singleton("Hi"), Sets.asUniqueSet(Arrays.asList("Hi", "Hi")));
        assertEquals(new HashSet<String>(Arrays.asList("Hi", "There")), Sets.asSet(Arrays.asList("Hi", "There")));
    }

    @Test
    public void testAsSetNull() {
        testNull((Object) null);
        testNull((Object[]) null);
        testNull(null, null);
        testNull(1, null);
        testNull(null, 2);
        testNull(1, null, 3);
    }

    private <T> void testNull(T... values) {
        testNullArray(values);
        testNullCollection(values == null ? null : Arrays.asList(values));
    }

    private <T> void testNullArray(T... values) {
        try {
            Sets.asSet(values);
            fail("testNullArray failed with values: " + Arrays.toString(values));
        } catch (IllegalArgumentException e) {
            if (values == null) {
                assertEquals("values is null", e.getMessage());
            } else {
                assertEquals("values contains null element: " + Arrays.toString(values), e.getMessage());
            }
        }
    }

    private <T> void testNullCollection(List<T> values) {
        try {
            Sets.asSet(values);
            fail("testNullCollection failed with values: " + values);
        } catch (IllegalArgumentException e) {
            if (values == null) {
                assertEquals("list is null", e.getMessage());
            } else {
                assertEquals("list contains null element: " + values, e.getMessage());
            }
        }
    }

    @Test
    public void testAsSortedSet() {
        assertEquals(Collections.<String>emptySet(), Sets.<String>asSortedSet());
        assertEquals(new HashSet<String>(), Sets.<String>asSortedSet());
        assertEquals(Collections.singleton("Hi"), Sets.asSortedSet("Hi"));
        assertEquals(Arrays.asList("Hi", "Me", "There"), new ArrayList<String>(Sets.asSortedSet("Hi", "There", "Me")));
    }

    @Test
    public void testIntersection() {
        Set<Integer> set1 = Sets.asSet(1, 2, 5);
        Set<Integer> set2 = Sets.asSet(2, 1, 7);
        Set<Integer> intersection = Sets.intersection(set1, set2);

        assertEquals(Sets.asSet(1, 2), intersection);
    }

    @Test
    public void testEmptyIntersection() {
        Set<Integer> set1 = Sets.asSet(1, 2, 5);
        Set<Integer> set2 = Sets.asSet(1, 2, 7);
        Set<Integer> set3 = Sets.asSet(3, 4);

        assertFalse(Sets.hasEmptyIntersection(set1, set2));
        assertTrue(Sets.hasEmptyIntersection(set1, set3));
    }

    @Test
    public void testSingleValue() {
        assertEquals("Hi", Sets.singleValue(Sets.asSet("Hi")));

        try {
            Sets.singleValue(Sets.asSet());
            fail();
        } catch (IllegalArgumentException e) {
            assertEquals("set should have 1 value but is empty", e.getMessage());
        }

        try {
            Sets.singleValue(Sets.asSet("Hi", "There"));
            fail();
        } catch (IllegalArgumentException e) {
            assertEquals("set should have 1 value but contains: [Hi, There]", e.getMessage());
        }
    }

    @Test
    public void testFormat() {

        assertEquals("Hallo\nblah\n42", Sets.formatSet(Sets.asSet("Hallo", "blah", "42")));

        assertEquals("[Hallo]\n[blah]\n[42]", Sets.formatSet(Sets.asSet("Hallo", "blah", "42"), new Converter<String, String>() {
            @Override
            public String convert(String source) {
                return "[" + source + "]";
            }
        }));

    }

}
