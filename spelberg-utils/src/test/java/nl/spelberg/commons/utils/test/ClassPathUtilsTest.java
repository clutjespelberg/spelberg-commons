package nl.spelberg.commons.utils.test;

import java.util.Set;
import nl.spelberg.commons.utils.collections.Sets;
import org.junit.Test;

import static org.junit.Assert.*;

public class ClassPathUtilsTest {

    @Test
    public void testFindAnnotatedClasses() {
        // setup
        String basePackage = getClass().getPackage().getName();

        // execute
        Set<Class<?>> classes = ClassPathUtils.findAnnotatedClasses(basePackage, ClassPathUtilsTestAnnotation.class);

        // verify
        assertEquals(Sets.asSet(ClassPathUtilsTestObjectOne.class, ClassPathUtilsTestObjectThree.class), classes);
    }

    @Test
    public void testFindAnnotatedMethodClasses() {
        // setup
        String basePackage = getClass().getPackage().getName();

        // execute
        Set<Class<?>> classes = ClassPathUtils.findAnnotatedMethodClasses(basePackage, ClassPathUtilsTestAnnotation.class);

        // verify
        assertEquals(Sets.asSet(ClassPathUtilsTestObjectOne.class, ClassPathUtilsTestObjectTwo.class), classes);
    }

    @Test
    public void testFindSubClasses() {
        // setup
        String basePackage = getClass().getPackage().getName();

        // execute
        Set<Class<?>> classes = ClassPathUtils.findSubClasses(basePackage, ClassPathUtilsTestBaseClass.class);

        // verify
        assertEquals(Sets.asSet(ClassPathUtilsTestBaseClass.class, ClassPathUtilsTestBaseClass.InnerSubClass.class, ClassPathUtilsTestSubClassOne.class,
                ClassPathUtilsTestSubClassOneSubOne.class, ClassPathUtilsTestSubClassTwo.class), classes);
    }

}
