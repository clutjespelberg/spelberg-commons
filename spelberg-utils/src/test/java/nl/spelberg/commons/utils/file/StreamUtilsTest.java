package nl.spelberg.commons.utils.file;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import org.junit.Test;

import static org.junit.Assert.*;

public class StreamUtilsTest {
    @Test
    public void testAsBytes() throws IOException {
        byte[] bytes = "Dit is een test".getBytes();
        assertArrayEquals(bytes, StreamUtils.asBytes(new ByteArrayInputStream(bytes)));
    }

    @Test
    public void testAsInputStream() throws IOException {
        byte[] bytes = "Dit is een test".getBytes();
        assertArrayEquals(bytes, StreamUtils.asBytes(StreamUtils.asInputStream(bytes)));
    }
}
