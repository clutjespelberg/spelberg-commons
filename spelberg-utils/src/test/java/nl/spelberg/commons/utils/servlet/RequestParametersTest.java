package nl.spelberg.commons.utils.servlet;

import org.junit.Test;

import static org.junit.Assert.*;

public class RequestParametersTest {

    public static final String MULTI_PART_SEPARATOR = "1234567890";
    public static final String NEW_LINE_SEPARATOR = "\r\n";

    @Test
    public void testAsHttpPostData() {
        RequestParameters requestParameters = new RequestParameters();
        assertEquals("", requestParameters.asHttpPostData(MULTI_PART_SEPARATOR, NEW_LINE_SEPARATOR));

        requestParameters.add("thisKey", "thisValue");
        assertEquals(expectedMultiPartFormData("thisKey", "thisValue"), requestParameters.asHttpPostData(MULTI_PART_SEPARATOR,
                NEW_LINE_SEPARATOR));

        requestParameters.add("otherKey", "otherValue");
        assertEquals(expectedMultiPartFormData("thisKey", "thisValue") + expectedMultiPartFormData("otherKey", "otherValue"),
                requestParameters.asHttpPostData(MULTI_PART_SEPARATOR, NEW_LINE_SEPARATOR));
    }

    private String expectedMultiPartFormData(String key, String value) {
        return "--1234567890\r\n" +
                "Content-Disposition: form-data; name=\"" + key + "\"\r\n" +
                "\r\n" +
                value + "\r\n";
    }
}
