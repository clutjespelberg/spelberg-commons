package nl.spelberg.commons.utils.validation;

import org.junit.Test;

import static org.junit.Assert.*;

public class ElfProefTest {

    @Test
    public void testIsValidNumbers() {
        assertTrue(ElfProef.BANKREKENING_9.isValid(7, 3, 6, 1, 6, 0, 2, 2, 1));
        assertTrue(ElfProef.BANKREKENING_10.isValid(0, 7, 3, 6, 1, 6, 0, 2, 2, 1));
        assertTrue(ElfProef.BANKREKENING_10_INVERSE.isValid(0, 7, 3, 6, 1, 6, 0, 2, 2, 1));
        assertTrue(ElfProef.BSN.isValid(1, 2, 3, 4, 5, 6, 7, 8, 2));
        assertFalse(ElfProef.BANKREKENING_9.isValid(7, 3, 6, 1, 6, 0, 2, 2, 2));
        assertFalse(ElfProef.BANKREKENING_10.isValid(0, 7, 3, 6, 1, 6, 0, 2, 2, 2));
        assertFalse(ElfProef.BANKREKENING_10_INVERSE.isValid(0, 7, 3, 6, 1, 6, 0, 2, 2, 2));
        assertFalse(ElfProef.BSN.isValid(1, 2, 3, 4, 5, 6, 7, 8, 3));
    }

    @Test
    public void testIsValidString() {
        assertTrue(ElfProef.BANKREKENING_9.isValid("736160221"));
        assertTrue(ElfProef.BANKREKENING_10.isValid("0736160221"));
        assertTrue(ElfProef.BANKREKENING_10_INVERSE.isValid("0736160221"));
        assertTrue(ElfProef.BSN.isValid("123456782"));
        assertFalse(ElfProef.BANKREKENING_9.isValid("836160221"));
        assertFalse(ElfProef.BANKREKENING_10.isValid("1736160221"));
        assertFalse(ElfProef.BANKREKENING_10_INVERSE.isValid("1736160221"));
        assertFalse(ElfProef.BSN.isValid("223456782"));
    }
}
