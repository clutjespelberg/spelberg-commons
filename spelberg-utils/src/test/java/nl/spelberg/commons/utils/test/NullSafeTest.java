package nl.spelberg.commons.utils.test;

import org.junit.Test;

import static org.junit.Assert.*;

public class NullSafeTest {

    @Test
    public void testValue() {
        assertEquals("", NullSafe.value(""));
        assertEquals("Hi", NullSafe.value("Hi"));

        Object value = new Object();
        assertSame(value, NullSafe.value(value));

        try {
            NullSafe.value(null);
            fail();
        } catch (IllegalArgumentException e) {
            assertEquals("value is null", e.getMessage());
        }

    }

    @Test
    public void testValueMessage() {

        assertEquals("", NullSafe.value("", "Message!"));
        assertEquals("Hi", NullSafe.value("Hi", "Message!"));

        Object value = new Object();
        assertSame(value, NullSafe.value(value, "Message!"));

        try {
            NullSafe.value(null, "Message!");
            fail();
        } catch (IllegalArgumentException e) {
            assertEquals("Message!", e.getMessage());
        }

    }

    @Test
    public void testCompareTo() {
        assertEquals(0, NullSafe.<Comparable>compareTo(null, null));
        assertEquals(-1, NullSafe.compareTo(null, ""));
        assertEquals(1, NullSafe.compareTo("", null));

        assertEquals(0, NullSafe.compareTo("", ""));
        assertEquals(-1, NullSafe.compareTo("1", "2"));
        assertEquals(1, NullSafe.compareTo("2", "1"));
    }
}
