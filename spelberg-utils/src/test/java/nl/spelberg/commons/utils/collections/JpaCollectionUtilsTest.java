package nl.spelberg.commons.utils.collections;

import java.util.ArrayList;
import java.util.List;
import nl.spelberg.commons.utils.dao.AbstractQueryEntity;
import org.junit.Test;
import static org.junit.Assert.*;

public class JpaCollectionUtilsTest {

    @Test
    public void testAsUniqueIdList() throws Exception {
        // setup
        List<MyEntity> sourceList = Arrays.asList(new MyEntity("3"), new MyEntity("5"), new MyEntity("7"), new MyEntity("5"), new MyEntity("2"));

        // execute
        List<MyEntity> resultList = JpaCollectionUtils.asUniqueIdList(sourceList);

        // verify
        List<MyEntity> expectedList = Arrays.asList(new MyEntity("3"), new MyEntity("5"), new MyEntity("7"), new MyEntity("2"));
        assertEquals(expectedList, resultList);
    }

    @Test
    public void testUpdateJpaList() {
        // hier geen Arrays.asList gebruiken, deze geeft namelijk een List die geen .remove heeft ondersteund
        List<String> existingValues = new ArrayList<String>();
        existingValues.addAll(Arrays.asList("1", "2", "4"));
        List<String> newValues = new ArrayList<String>();
        newValues.addAll(Arrays.asList("2", "3", "4", "5"));
        JpaCollectionUtils.updateJpaList(existingValues, newValues, new TestUpdateFactory());

        assertEquals(existingValues, newValues);

    }

    private static class TestUpdateFactory implements JpaCollectionUtils.UpdateFactory<String, String> {
        @Override
        public boolean equalsExistingValue(String existingValue, String newValue) {
            return existingValue.equals(newValue);
        }

        @Override
        public String createExistingValue(String newValue) {
            return newValue;
        }
    }


    private static class MyEntity extends AbstractQueryEntity {
        private final String id;

        private MyEntity(String id) {
            this.id = id;
        }

        @Override
        public boolean equalsId(Object obj) {
            return equals(obj);
        }
    }
}
