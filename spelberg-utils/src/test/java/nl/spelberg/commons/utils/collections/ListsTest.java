package nl.spelberg.commons.utils.collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import org.junit.Test;
import org.springframework.core.convert.converter.Converter;
import static org.junit.Assert.*;

public class ListsTest {

    @Test
    public void testList() {
        assertEquals(Arrays.asList("1", "2", "3"), Lists.convert(Arrays.asList(1, 2, 3), new Converter<Integer, String>() {
            @Override
            public String convert(Integer source) {
                return String.valueOf(source);
            }
        }));
        assertEquals(Arrays.asList("1", "2", "3"), Lists.convert(new Integer[]{1, 2, 3}, new Converter<Integer, String>() {
            @Override
            public String convert(Integer source) {
                return String.valueOf(source);
            }
        }));
    }

    @Test
    public void testPrepend() {
        assertEquals(Arrays.asList("Hi"), Lists.prepend("Hi", Collections.<String>emptyList()));
        assertEquals(Arrays.asList("Hi", "There"), Lists.prepend("Hi", Arrays.asList("There")));
        assertEquals(Arrays.asList("Hi", "There", "!"), Lists.prepend("Hi", Arrays.asList("There", "!")));
    }

    @Test
    public void testToNullFilteredList() {
        String[] array = {null, "Hi", null, "There"};
        assertEquals(Arrays.asList("Hi", "There"), Lists.toNullFilteredList(array));
    }

    @Test
    public void testReverse() {
        assertEquals(Arrays.asList("three", "two", "one"), Lists.reverse(Arrays.asList("one", "two", "three")));
    }

    @Test
    public void testMergeSorted() throws Exception {
        assertEquals(Arrays.asList("1", "1", "10", "100", "16", "2", "4", "8"),
                Lists.mergeSorted(Arrays.asList("1", "2", "16", "4", "8"), Arrays.asList("10", "1", "100")));

        assertEquals(Arrays.asList("1", "1", "10", "100", "16", "2", "4", "8"),
                Lists.mergeSorted(Arrays.asList("1", "2", "16", "4", "8"), Arrays.asList("10", "1", "100"), new Comparator<Object>() {
                    @Override
                    public int compare(Object o1, Object o2) {
                        if (o1 instanceof String && o2 instanceof String) {
                            return ((String) o1).compareTo((String) o2);
                        } else {
                            return 0;
                        }
                    }
                }));
    }

    @Test
    public void testAsList() {
        assertEquals(new ArrayList<String>(), Lists.<String>asList());
        assertEquals(Arrays.asList("42"), Lists.asList("42"));
        assertEquals(Arrays.asList("4", "2"), Lists.asList("4", "2"));
        assertEquals(Arrays.asList(4, 2), Lists.asList(4, 2));
    }

    @Test
    public void testAsUniqueList() {
        assertEquals(Lists.asList("2", "5", "1"), Lists.asUniqueList(Lists.asList("2", "5", "1", "5", "1")));
    }

    @Test
    public void testJoin() {
        assertEquals(Lists.<String>asList(), Lists.<String>join());
        assertEquals(Lists.<String>asList(), Lists.<String>join(Lists.<String>asList(), Lists.<String>asList(), Lists.<String>asList()));
        assertEquals(Lists.<String>asList("3", "2", "5"), Lists.<String>join(Lists.<String>asList("3"), Lists.<String>asList("2"), Lists.<String>asList("5")));
        assertEquals(Lists.<String>asList("3", "2", "5"), Lists.<String>join(Lists.<String>asList("3"), Lists.<String>asList("2", "5")));
        assertEquals(Lists.<String>asList("3", "2", "5"), Lists.<String>join(Lists.<String>asList("3", "2"), Lists.<String>asList("5")));
    }

}
