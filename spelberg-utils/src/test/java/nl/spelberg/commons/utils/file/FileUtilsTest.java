package nl.spelberg.commons.utils.file;

import org.junit.Test;

import static org.junit.Assert.*;

public class FileUtilsTest {

    @Test
    public void testPathFromClass() throws Exception {
        assertEquals(null, FileUtils.pathFromClass(null));
        assertEquals("java/lang/Object", FileUtils.pathFromClass(Object.class));
        assertEquals("java/lang/Integer", FileUtils.pathFromClass(Integer.class));
        assertEquals("nl/spelberg/commons/utils/file/FileUtilsTest", FileUtils.pathFromClass(getClass()));

        assertEquals(null, FileUtils.pathFromObject(null));
        assertEquals("nl/spelberg/commons/utils/file/FileUtilsTest", FileUtils.pathFromObject(this));
    }
}
