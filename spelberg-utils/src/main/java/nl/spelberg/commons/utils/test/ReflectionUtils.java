package nl.spelberg.commons.utils.test;

import java.beans.ExceptionListener;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.springframework.util.Assert;

/**
 * Part copied from {@link java.beans.ReflectionUtils} and generified, other methods added.
 */
public class ReflectionUtils {

    /**
     * Copied from {@link java.beans.ReflectionUtils#isPrimitive(Class)}.
     */
    public static boolean isPrimitive(Class<?> type) {
        return primitiveTypeFor(type) != null;
    }

    /**
     * Copied from {@link java.beans.ReflectionUtils#primitiveTypeFor(Class)}.
     */
    public static Class<?> primitiveTypeFor(Class<?> wrapper) {
        if (wrapper == Boolean.class) {
            return Boolean.TYPE;
        }
        if (wrapper == Byte.class) {
            return Byte.TYPE;
        }
        if (wrapper == Character.class) {
            return Character.TYPE;
        }
        if (wrapper == Short.class) {
            return Short.TYPE;
        }
        if (wrapper == Integer.class) {
            return Integer.TYPE;
        }
        if (wrapper == Long.class) {
            return Long.TYPE;
        }
        if (wrapper == Float.class) {
            return Float.TYPE;
        }
        if (wrapper == Double.class) {
            return Double.TYPE;
        }
        if (wrapper == Void.class) {
            return Void.TYPE;
        }
        return null;
    }

    /**
     * Copied from {@link java.beans.ReflectionUtils#getPrivateField(Object, Class, String, java.beans.ExceptionListener)}.
     */
    public static <T, F> F getPrivateField(T instance, Class<? extends T> cls, String name, ExceptionListener el) {
        try {
            Field f = cls.getDeclaredField(name);
            f.setAccessible(true);
            @SuppressWarnings("unchecked")
            F value = (F) f.get(instance);
            return value;
        } catch (Exception e) {
            if (el != null) {
                el.exceptionThrown(e);
            }
        }
        return null;
    }

    public static <T, F> F getPrivateInstanceField(T instance, String name, ExceptionListener el) {
        return getPrivateField(instance, instance.getClass(), name, el);
    }

    public static <T, F> F getPrivateStaticField(Class<T> tClass, String name, ExceptionListener el) {
        return getPrivateField(null, tClass, name, el);
    }

    public static void setPrivateField(Object instance, String fieldName, Object value) {
        try {
            Field f = getDeclaredField(instance.getClass(), fieldName);
            f.setAccessible(true);
            f.set(instance, value);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public static <T, A extends Annotation> void setPrivateField(T instance, Class<A> annotationClass, Object value) throws NoSuchFieldException {
        setPrivateField(getPrivateField(instance, Annotation.class), instance, value);
    }

    public static <T, A extends Annotation> Field getPrivateField(T instance, Class<A> annotationClass) throws NoSuchFieldException {
        Assert.notNull(instance, "instance is null");
        Class<?> instanceClass = instance.getClass();

        Class<?> declaringClass = instanceClass;
        while (declaringClass != null) {

            Field[] declaredFields = declaringClass.getDeclaredFields();
            for (Field declaredField : declaredFields) {
                A annotation = declaredField.getAnnotation(annotationClass);
                if (annotation != null) {
                    return declaredField;
                }
            }

            declaringClass = declaringClass.getSuperclass();
        }

        throw new NoSuchFieldException("No private field annotated with '" + annotationClass.getName() + "' exists in class '" + instanceClass +
                "', instance: " + instance);
    }

    public static <T> void setPrivateField(Field declaredField, T instance, Object value) {
        try {
            declaredField.setAccessible(true);
            declaredField.set(instance, value);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public static Field getDeclaredField(final Class<?> cls, final String fieldName) {
        if (cls == null) {
            throw new IllegalArgumentException("cls is null");
        }
        if (fieldName == null) {
            throw new IllegalArgumentException("fieldName is null");
        }
        Class<?> declaringClass = cls;
        while (declaringClass != null) {
            try {
                return declaringClass.getDeclaredField(fieldName);
            } catch (NoSuchFieldException e) {
                declaringClass = declaringClass.getSuperclass();
            }
        }
        throw new IllegalArgumentException("Field " + cls.getSimpleName() + "." + fieldName + " does not exist.");
    }

    public static List<Field> getPrivateFields(Object instance) {
        Assert.notNull(instance, "o is null");
        Class<?> instanceClass = instance.getClass();
        return getFields(instanceClass);
    }

    public static List<Field> getFields(Class<?> instanceClass) {
        if (instanceClass == null ||  instanceClass.equals(Object.class)) {
            return new ArrayList<Field>();
        } else {
            List<Field> fields = new ArrayList<Field>();

            // all superclass fields
            fields.addAll(getFields(instanceClass.getSuperclass()));

            // all instance fields
            Collections.addAll(fields, instanceClass.getDeclaredFields());

            return fields;
        }
    }
}
