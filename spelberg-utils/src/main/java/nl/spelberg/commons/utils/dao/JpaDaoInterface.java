package nl.spelberg.commons.utils.dao;

import java.util.List;

public interface JpaDaoInterface<ENTITY, ID> {

    List<ENTITY> findAll();

    ENTITY persist(ENTITY entity);

    ENTITY get(ID id);

    ENTITY find(ID id);

    void persistMany(Iterable<ENTITY> entities);

    void delete(ENTITY entity);

    /**
     * TODO: uitzoeken waarom dit nodig is en het oplossen
     */
    void flush();

    void deleteMany(List<ENTITY> entities);

    void deleteById(ID id);
}
