package nl.spelberg.commons.utils.ui;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class JsonError extends AbstractJsonObject {
    private final String message;
    private final String title;
    private final String severity;

    public JsonError(String severity, String title, String message) {
        this.message = message;
        this.title = title;
        this.severity = severity;
    }

    public JsonError(UIException.Severity severity, String title, String message) {
        this(severity.toString(), title, message);
    }

    public JsonError(UIException uiException) {
        this(uiException.getSeverity(), uiException.getTitle(), uiException.getMessage());
    }
}
