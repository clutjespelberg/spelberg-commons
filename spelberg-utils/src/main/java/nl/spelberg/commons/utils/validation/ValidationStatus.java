package nl.spelberg.commons.utils.validation;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.util.Assert;

public class ValidationStatus {

    private final Set<ValidationStatusId> ids;

    public ValidationStatus(Collection<ValidationStatusId> ids) {
        Assert.notEmpty(ids, "ids is null or empty");
        this.ids = new LinkedHashSet<ValidationStatusId>(ids);
    }

    public ValidationStatus(ValidationStatusId... ids) {
        Assert.notNull(ids, "ids is null");
        Set<ValidationStatusId> idSet = new LinkedHashSet<ValidationStatusId>();
        Collections.addAll(idSet, ids);
        this.ids = idSet;
    }

    public boolean isOk() {
        return ids.isEmpty();
    }

    public Set<ValidationStatusId> getIds() {
        return ids;
    }

    public void add(ValidationStatusId validationStatusId) {
        ids.add(validationStatusId);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ValidationStatus that = (ValidationStatus) o;

        if (ids != null ? !ids.equals(that.ids) : that.ids != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return ids != null ? ids.hashCode() : 0;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
