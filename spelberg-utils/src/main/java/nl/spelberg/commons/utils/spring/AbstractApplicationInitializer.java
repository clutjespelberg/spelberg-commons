package nl.spelberg.commons.utils.spring;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

@Transactional
public abstract class AbstractApplicationInitializer implements ApplicationListener<ContextRefreshedEvent> {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private boolean isInitialized = false;

    private boolean doGenerateTestData = false;

    private final String applicationName;

    protected AbstractApplicationInitializer(String applicationName) {
        Assert.hasText(applicationName, "applicationName bevat geen text");
        this.applicationName = applicationName;
    }

    public void setDoGenerateTestData(boolean doGenerateTestData) {
        this.doGenerateTestData = doGenerateTestData;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {

        // this method can be called several times on startup:
        // - when the spring ContextLoaderListener is started the applicationcontext is initialized
        // - when the spring DispatcherServlet is started the applicationcontest is refreshed, adding the web context

        if (initializeNeeded()) {
            log.info("Initializing " + applicationName + " ...");
            long startMillis = System.currentTimeMillis();

            doInitialize(doGenerateTestData);

            long durationMillis = System.currentTimeMillis() - startMillis;
            log.info("Initializing " + applicationName + " finished in " + durationMillis + " ms");
        } else {
            log.debug("Initializing " + applicationName + " not needed, already initialized.");
        }
    }

    private synchronized boolean initializeNeeded() {
        if (!this.isInitialized) {
            this.isInitialized = true;
            return true;
        } else {
            return false;
        }
    }

    protected abstract void doInitialize(boolean generateTestData);

}
