package nl.spelberg.commons.utils.test;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import nl.spelberg.commons.utils.collections.Lists;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;
import org.springframework.core.type.filter.AbstractTypeHierarchyTraversingFilter;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.core.type.filter.AssignableTypeFilter;
import org.springframework.core.type.filter.TypeFilter;
import org.springframework.util.Assert;

public class ClassPathUtils {

    public static final Comparator<Class> CLASS_COMPARATOR = new Comparator<Class>() {
        @Override
        public int compare(Class class1, Class class2) {
            return class1.getName().compareTo(class2.getName());
        }
    };

    public static <T extends Annotation> Set<Class<?>> findAnnotatedClasses(String basePackage, Class<T>... annotationClasses) {
        Assert.notEmpty(annotationClasses, "annotatedClasses is empty");
        List<TypeFilter> typeFilters = Lists.convert(annotationClasses, new AnnotationTypeFilterConverter<T>());
        return findClasses(basePackage, typeFilters);
    }

    public static <T extends Annotation> Set<Class<?>> findAnnotatedMethodClasses(String basePackage,
            Class<T>... annotationClasses) {
        Assert.notEmpty(annotationClasses, "annotatedClasses is empty");
        List<TypeFilter> typeFilters = Lists.convert(annotationClasses, new MethodAnnotationTypeFilterConverter<T>());
        return findClasses(basePackage, typeFilters);
    }

    public static Set<Class<?>> findSubClasses(final String basePackage, final Class<?> baseClass) {
        Assert.notNull(baseClass, "baseClass is null");
        final List<TypeFilter> typeFilters = Arrays.<TypeFilter>asList(new AssignableTypeFilter(baseClass) {
            @Override
            public boolean match(MetadataReader metadataReader, MetadataReaderFactory metadataReaderFactory) throws IOException {
                try {
                    return super.match(metadataReader, metadataReaderFactory);
                } catch (Exception e) {
                    // ignore
                    return false;
                }
            }

            @Override
            protected boolean matchSelf(MetadataReader metadataReader) {
                return super.matchSelf(metadataReader);
            }
        });
        return findClasses(basePackage, typeFilters);
    }

    private static Set<Class<?>> findClasses(String basePackage, List<TypeFilter> typeFilters) {
        Assert.hasText(basePackage, "basePackage not specified: basePackage='" + basePackage + "'");
        ClassPathScanningCandidateComponentProvider componentProvider = new ClassPathScanningCandidateComponentProvider(false);
        for (TypeFilter typeFilter : typeFilters) {
            componentProvider.addIncludeFilter(typeFilter);
        }
        Set<BeanDefinition> candidateComponents = componentProvider.findCandidateComponents(basePackage);

        TreeSet<Class<?>> annotatedClasses = new TreeSet<Class<?>>(CLASS_COMPARATOR);
        for (BeanDefinition beanDefinition : candidateComponents) {
            String beanClassName = beanDefinition.getBeanClassName();
            try {
                Class<?> beanClass = Class.forName(beanClassName);
                annotatedClasses.add(beanClass);
            } catch (ClassNotFoundException e) {
                throw new RuntimeException(e.getMessage(), e);
            }
        }

        return annotatedClasses;
    }

    private static class AnnotationTypeFilterConverter<T extends Annotation> implements Converter<Class<T>, TypeFilter> {
        @Override
        public TypeFilter convert(Class<T> annotationClass) {
            return new AnnotationTypeFilter(annotationClass);
        }
    }

    private static class MethodAnnotationTypeFilterConverter<T extends Annotation> implements Converter<Class<T>, TypeFilter> {
        @Override
        public TypeFilter convert(Class<T> annotationClass) {
            final String annotationClassName = annotationClass.getName();
            return new AbstractTypeHierarchyTraversingFilter(false, false) {
                @Override
                protected boolean matchSelf(MetadataReader metadataReader) {
                    AnnotationMetadata metadata = metadataReader.getAnnotationMetadata();
                    return metadata.isConcrete() && metadata.hasAnnotatedMethods(annotationClassName);
                }
            };
        }
    }

}
