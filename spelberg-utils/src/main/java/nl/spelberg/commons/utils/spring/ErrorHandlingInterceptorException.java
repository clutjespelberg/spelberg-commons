package nl.spelberg.commons.utils.spring;

public class ErrorHandlingInterceptorException extends RuntimeException {

    public ErrorHandlingInterceptorException(String message) {
        super(message);
    }

}
