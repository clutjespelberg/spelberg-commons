package nl.spelberg.commons.utils.collections;

import java.util.AbstractMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import org.springframework.core.convert.converter.Converter;

public class Maps {

    /**
     * Formats a map to a multiline string.
     */
    public static String formatMap(Map<?, ?> map) {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<?, ?> entry : map.entrySet()) {
            sb.append(entry.getKey());
            sb.append(" -> ");
            sb.append(entry.getValue());
            sb.append("\n");
        }
        return sb.toString();
    }

    /**
     * Formats a map to a multiline string using a custom converter.
     */
    public static <K, V> String formatMap(Map<K, V> map, Converter<Map.Entry<K, V>, String> converter) {
        StringBuilder sb = null;
        for (Map.Entry<K, V> entry : map.entrySet()) {
            if (sb == null) {
                sb = new StringBuilder();
            } else {
                sb.append("\n");
            }
            sb.append(converter.convert(entry));
        }
        return sb == null ? "" : sb.toString();
    }

    public static <K, V> Map<K, V> map(K key, V value) {
        @SuppressWarnings("unchecked")
        Map<K, V> map = map(entry(key, value));
        return map;
    }

    public static <K, V> Map<K, V> map(Map.Entry<? extends K, ? extends V>... entries) {
        Map<K, V> map = new LinkedHashMap<K, V>();
        for (Map.Entry<? extends K, ? extends V> entry : entries) {
            map.put(entry.getKey(), entry.getValue());
        }
        return map;
    }

    public static <K extends Comparable<K>, V> Map<K, V> sortedMap(Map.Entry<K, V>... entries) {
        SortedMap<K, V> map = new TreeMap<K, V>();
        for (Map.Entry<K, V> entry : entries) {
            map.put(entry.getKey(), entry.getValue());
        }
        return map;
    }

    public static <K, V> Map.Entry<K, V> entry(K key, V value) {
        return new AbstractMap.SimpleEntry<K, V>(key, value);
    }

    /**
     * Convert a collection to a map by generating the map key based on the value.
     *
     * @param values              The collection
     * @param valueToKeyConverter The converter to create a key from a value
     * @return a map in the same order as the collection, the size can be smaller when duplicate keys exist
     */
    public static <K, V> Map<K, V> asMap(Iterable<V> values, Converter<V, K> valueToKeyConverter) {
        Map<K, V> map = new LinkedHashMap<K, V>();
        for (V value : values) {
            K key = valueToKeyConverter.convert(value);
            map.put(key, value);
        }
        return map;
    }

    public static <K, V> Set<V> asUniqueValueSet(Map<K, V> map) {
        return new LinkedHashSet<V>(map.values());
    }

}
