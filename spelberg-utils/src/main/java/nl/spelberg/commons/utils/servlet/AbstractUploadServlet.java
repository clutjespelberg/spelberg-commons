package nl.spelberg.commons.utils.servlet;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import nl.spelberg.commons.utils.file.StreamUtils;
import nl.spelberg.commons.utils.validation.JsonValidationException;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

public abstract class AbstractUploadServlet extends HttpServlet {

    private static final Logger log = LoggerFactory.getLogger(AbstractUploadServlet.class);

    public static final String OVERWRITE_PARAM = "uploadOverwrite";

    @Override
    public void init(ServletConfig config) throws ServletException {
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, config.getServletContext());
        super.init(config);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        boolean isMultipart = ServletFileUpload.isMultipartContent(request);

        if (isMultipart) {
            ServletFileUpload upload = new ServletFileUpload();
            try {
                FileItemIterator iter = upload.getItemIterator(request);
                RequestParameters requestParameters = new RequestParameters();

                byte[] bytes = null;
                while (iter.hasNext()) {
                    FileItemStream item = iter.next();
                    if (!item.isFormField()) {
                        // de data uit het item moet direct ingelezen worden anders wordt deze overgeslagen, zie javadoc van item.openStream()
                        Assert.state(bytes == null, "bytes moet hier null zijn");
                        bytes = StreamUtils.asBytes(item.openStream());
                    } else {
                        requestParameters.addMultiPartFormItem(item);
                    }
                }
                Assert.state(bytes != null, "bytes moet een waarde bevatten");
                processInputStream(StreamUtils.asInputStream(bytes), requestParameters, response);
            } catch (FileUploadException e) {
                throw new IOException("Error processing upload request", e);
            }
        }
    }

    public abstract void processInputStream(InputStream input, RequestParameters requestParameters, HttpServletResponse response);

    public Date getProcessTimestamp() {
        return new Date();
    }

    protected void printUploadStatusMessageOK(HttpServletResponse response, int successfulLines, int totalLines) {
        try {
            PrintWriter writer = response.getWriter();
            writer.print("<div class=\"uploadStatusMessage\">" + successfulLines + "/" + totalLines +
                    " zijn ge&iuml;mporteerd.</div>\n");
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    protected void printUploadStatusMessageError(HttpServletResponse response, String message, int lineNumber) {
        try {
            PrintWriter writer = response.getWriter();
            writer.print("<div class=\"uploadErrorMessage\">Fout in regel " + lineNumber + ": " + message + "</div>\n<hr/>\n");
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    protected void printUploadStatusMessageError(HttpServletResponse response, Throwable cause, int lineNumber) {
        if (cause instanceof JsonValidationException) {
            log.debug(cause.getMessage(), cause);
        } else {
            log.error("UPLOAD: Fout in regel " + lineNumber + ": " + cause.getMessage(), cause);
        }
        printUploadStatusMessageError(response, cause.getMessage(), lineNumber);
    }

}
