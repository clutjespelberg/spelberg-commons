package nl.spelberg.commons.utils.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import nl.spelberg.commons.utils.ui.AbstractSerializable;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.Assert;

public class RequestParameters extends AbstractSerializable {

    private final Map<String, List<String>> params = new LinkedHashMap<String, List<String>>();

    public RequestParameters() {
    }

    public RequestParameters(Map<String, List<String>> params) {
        Assert.notNull(params, "params is null");
        add(params);
    }

    public RequestParameters(String singleKey, String singleValue) {
        Assert.notNull(singleKey, "singleKey is null");
        Assert.notNull(singleValue, "singleValue is null");
        add(singleKey, singleValue);
    }

    public final List<String> get(String key) {
        Assert.notNull(key, "key is null");
        List<String> values = params.get(key);
        if (values == null) {
            return Collections.emptyList();
        } else {
            return values;
        }
    }

    public final String getValue(String key) {
        List<String> list = get(key);
        if (list.size() == 1) {
            return list.get(0);
        } else {
            return null;
        }
    }

    public RequestParameters add(String key, String value) {
        if (params.containsKey(key)) {
            params.get(key).add(value);
        } else {
            List<String> values = new ArrayList<String>();
            values.add(value);
            params.put(key, values);
        }
        return this;
    }

    public boolean getBoolean(String key) {
        List<String> list = get(key);
        if (list != null && list.size() == 1) {
            return Boolean.parseBoolean(list.get(0));
        } else {
            return false;
        }
    }

    public boolean equalsValue(String key, String value) {
        String actualValue = getValue(key);
        return value == null ? actualValue == null : value.equals(actualValue);
    }

    public String asHttpPostData(String boundary, String newLineSeparator) {
        if (params.isEmpty()) {
            return "";
        } else {
            ArrayList<String> parts = new ArrayList<String>();

            for (Map.Entry<String, List<String>> keyValue : params.entrySet()) {
                String part = "--" + boundary + newLineSeparator +
                        "Content-Disposition: form-data; name=\"" + keyValue.getKey() + "\"" + newLineSeparator +
                        newLineSeparator +
                        StringUtils.join(keyValue.getValue(), "&") + newLineSeparator;
                parts.add(part);
            }
            return StringUtils.join(parts, "");
        }
    }

    public void addMultiPartFormItem(FileItemStream fileItemStream) throws IOException {
        Assert.isTrue(fileItemStream.isFormField(), "fileItemStream is geen form field");

        String parameterName = fileItemStream.getFieldName();
        Assert.hasText(parameterName, "parameterName bevat geen text");

        BufferedReader reader = new BufferedReader(new InputStreamReader(fileItemStream.openStream()));
        String parameterValue = reader.readLine();

        add(parameterName, parameterValue);
    }

    public RequestParameters join(RequestParameters other) {
        return new RequestParameters(this.params).add(other.params);
    }

    private RequestParameters add(Map<String, List<String>> params) {
        this.params.putAll(params);
        return this;
    }
}
