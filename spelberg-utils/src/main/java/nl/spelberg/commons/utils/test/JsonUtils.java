package nl.spelberg.commons.utils.test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import nl.spelberg.commons.utils.ui.AbstractJsonObject;
import org.springframework.util.Assert;

public class JsonUtils {

    public static JsonField jsonField(String fieldName, Object value) {
        return new JsonField(fieldName, value);
    }

    public static <T extends AbstractJsonObject> T jsonCreate(Class<T> tClass, JsonField... fields) {
        try {

            // create instance
            Constructor<T> constructor = tClass.getDeclaredConstructor();
            constructor.setAccessible(true);
            T jsonObject = constructor.newInstance();

            // set fields
            for (JsonField field : fields) {
                field.applyTo(jsonObject);
            }

            // validate
            jsonObject.jsonValidate();

            // and return
            return jsonObject;

        } catch (InstantiationException e) {
            throw new RuntimeException(e.getMessage(), e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e.getMessage(), e);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e.getMessage(), e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public static class JsonField {
        private String fieldName;
        private Object value;

        public JsonField(String fieldName, Object value) {
            Assert.hasText(fieldName, "fieldName bevat geen text");
            this.fieldName = fieldName;
            this.value = value;
        }

        public void applyTo(Object object) {
            ReflectionUtils.setPrivateField(object, fieldName, value);
        }
    }
}
