package nl.spelberg.commons.utils.collections;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import org.springframework.core.convert.converter.Converter;
import org.springframework.util.Assert;

public class Sets {

    public static <T> Set<T> asSet(T... values) {
        Assert.notNull(values, "values is null");
        LinkedHashSet<T> set = new LinkedHashSet<T>();
        for (T element : values) {
            Assert.notNull(element, "values contains null element: " + Arrays.toString(values));
            set.add(element);
        }
        return set;
    }

    /**
     * @param list list, mag geen dubbele waarden bevatten
     * @return de list als set, waarbij de volgorde behouden wordt
     * @throws IllegalArgumentException als de list dubbele waarden bevat
     * @see #asUniqueSet(java.util.List)
     */
    public static <T> Set<T> asSet(List<T> list) {
        Set<T> set = asUniqueSet(list);
        Assert.isTrue(list.size() == set.size(), "list bevat dubbele waarden: " + list);
        return set;
    }

    /**
     * @param list list, mag dubbele waarden bevatten
     * @return de unieke set uit een list, ofwel de meegegeven list ontdaan van alle dubbelen, originele volgorde niet gegarandeerd
     * @see #asSet
     */
    public static <T> Set<T> asUniqueSet(List<T> list) {
        Assert.notNull(list, "list is null");
        LinkedHashSet<T> set = new LinkedHashSet<T>();
        for (T next : list) {
            Assert.notNull(next, "list contains null element: "+ list);
            set.add(next);
        }
        return set;
    }

    public static <T extends Comparable<T>> SortedSet<T> asSortedSet(T... values) {
        SortedSet<T> set = new TreeSet<T>();
        Collections.addAll(set, values);
        return set;
    }

    public static <T extends Comparable<T>> SortedSet<T> asSortedSet(List<T> list) {
        Assert.notNull(list, "list is null");
        SortedSet<T> set = new TreeSet<T>();
        for (T element : list) {
            Assert.notNull(element, "list contains null element: " + list);
            set.add(element);
        }
        return set;
    }

    /**
     * @return de intersectie van 2 sets, ofwel alle elementen die
     *         zowel in set 1 als in set 2 voorkomen
     */
    public static <T> Set<T> intersection(Set<T> set1, Set<T> set2) {
        Assert.notNull(set1, "set1 is null");
        Assert.notNull(set2, "set2 is null");
        Set<T> temp = new LinkedHashSet<T>(set1);
        temp.retainAll(set2);
        return temp;
    }

    /**
     * @return true als de intersection van  set 1 en set 2 leeg is (dwz,
     *         er zijn geen elementen die in beide sets voorkomen)
     */
    public static <T> boolean hasEmptyIntersection(Set<T> set1, Set<T> set2) {
        return intersection(set1, set2).isEmpty();
    }

    public static <T> T singleValue(Set<T> set) {
        Assert.notNull(set, "set is null");
        Assert.notEmpty(set, "set should have 1 value but is empty");
        Assert.isTrue(set.size() == 1, "set should have 1 value but contains: " + set);
        return set.iterator().next();
    }

    /**
     * Formats a set to a multiline string.
     */
    public static String formatSet(Set<?> set) {
        StringBuilder sb = null;
        for (Object value : set) {
            if (sb == null) {
                sb = new StringBuilder();
            } else {
                sb.append("\n");
            }
            sb.append(value);
        }
        return sb == null ? "" : sb.toString();
    }

    /**
     * Formats a set to a multiline string using a custom converter.
     */
    public static <T> String formatSet(Set<T> set, Converter<? super T, String> converter) {
        StringBuilder sb = null;
        for (T value : set) {
            if (sb == null) {
                sb = new StringBuilder();
            } else {
                sb.append("\n");
            }
            sb.append(converter.convert(value));
        }
        return sb == null ? "" : sb.toString();
    }

}
