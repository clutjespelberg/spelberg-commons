package nl.spelberg.commons.utils.ui;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import nl.spelberg.commons.utils.validation.JsonValidationException;

/**
 * Class for default settings for serialization (JAX-B, HttpInvoke)
 * This has to be a class otherwise the annotation do not propagate
 *
 * @author Pieter Bruining <pieter.bruining@spelberg.nl>
 */
@SuppressWarnings({"UnusedDeclaration", "FieldCanBeLocal"})
@XmlAccessorType(XmlAccessType.FIELD)
public abstract class AbstractJsonObject extends AbstractSerializable {

    /**
     * Valideert de inhoud van de XmlSerializable, dit is zinvol als dit binnenkomende data is die door middel van reflection
     * in de private variabelen is gezet.
     *
     * De default implementatie gooit standaard een {@link nl.spelberg.commons.utils.validation.JsonValidationException} om te voorkomen dat niet correct
     * gevalideerde
     * objecten in de backent gebruikt gaan worden.
     *
     * @throws nl.spelberg.commons.utils.validation.JsonValidationException
     *          als de inhoud niet valide is.
     */
    public void jsonValidate() throws JsonValidationException {
        // TODO validatie implementeren met JSR-303 validation
        throw new JsonValidationException("Melding", "Validatie niet aanwezig voor '" + getClass().getSimpleName() + "'");
    }

}
