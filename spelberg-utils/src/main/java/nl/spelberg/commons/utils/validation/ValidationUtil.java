package nl.spelberg.commons.utils.validation;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.List;
import nl.spelberg.commons.utils.test.ReflectionUtils;
import nl.spelberg.commons.utils.ui.AbstractJsonObject;

public class ValidationUtil {

    public static void isFalse(boolean condition, String userMessage) {
        if (condition) {
            throw new JsonValidationException(userMessage);
        }
    }

    public static void isTrue(boolean condition, String userMessage) {
        isFalse(!condition, userMessage);
    }

    public static void notNull(Object o, String fieldName) {
        isFalse(o == null, fieldName + " is null");
    }

    public static void notBlank(String value, String fieldName) {
        notNull(value, fieldName);
        isFalse(value.trim().isEmpty(), fieldName + " is blank");
    }

    public static void notEmpty(Collection<?> c, String fieldName) {
        notNull(c, fieldName);
        isFalse(c.isEmpty(), fieldName + " is empty");
    }

    public static void notNullValidate(AbstractJsonObject jsonObject, String fieldName) {
        notNull(jsonObject, fieldName);
        try {
            jsonObject.jsonValidate();
        } catch (JsonValidationException e) {
            throw new JsonValidationException(fieldName, e);
        }
    }

    public static <T extends AbstractJsonObject> void notNullValidate(List<T> list, String fieldName) {
        notNull(list, fieldName);
        for (int i = 0; i < list.size(); i++) {
            T item = list.get(i);
            notNullValidate(item, fieldName + "[" + i + "]");
        }
    }

    public static <T extends AbstractJsonObject> void notEmptyValidate(List<T> list, String fieldName) {
        notEmpty(list, fieldName);
        for (int i = 0; i < list.size(); i++) {
            T item = list.get(i);
            notNullValidate(item, fieldName + "[" + i + "]");
        }
    }

    public static void nullOrValidate(AbstractJsonObject jsonObject, String fieldName) {
        try {
            if (jsonObject != null) {
                jsonObject.jsonValidate();
            }
        } catch (JsonValidationException e) {
            throw new JsonValidationException(fieldName, e);
        }
    }

    public static void hasText(String value, String fieldName) {
        notNull(value, fieldName);
        value = value.trim();
        isFalse(value.isEmpty(), fieldName + " moet een waarde hebben");
    }

    public static <T extends Comparable<T>> void lt(T value, T comparison, String fieldName) {
        notNull(value, fieldName);
        isTrue(value.compareTo(comparison) < 0, fieldName + " moet kleiner zijn dan " + comparison);
    }

    public static <T extends Comparable<T>> void lte(T value, T comparison, String fieldName) {
        notNull(value, fieldName);
        isTrue(value.compareTo(comparison) <= 0, fieldName + " moet kleiner of gelijk zijn aan " + comparison);
    }

    public static <T extends Comparable<T>> void gt(T value, T comparison, String fieldName) {
        notNull(value, fieldName);
        isTrue(value.compareTo(comparison) > 0, fieldName + " moet groter zijn dan " + comparison);
    }

    public static <T extends Comparable<T>> void gte(T value, T comparison, String fieldName) {
        notNull(value, fieldName);
        isTrue(value.compareTo(comparison) >= 0, fieldName + " moet groter of gelijk zijn aan " + comparison);
    }

    /**
     * Valideert alle velden van een object, afhankelijk van het type wordt de juiste validate method aangeroepen.
     */
    public static void validateAllFields(Object o) {
        List<Field> fields = ReflectionUtils.getPrivateFields(o);
        for (Field field : fields) {
            Class<?> fieldType = field.getType();
            String fieldName = field.getName();
            try {
                field.setAccessible(true);
                Object fieldValue = field.get(o);
                if (AbstractJsonObject.class.isAssignableFrom(fieldType)) {
                    notNullValidate((AbstractJsonObject) fieldValue, fieldName);
                } else if (String.class.isAssignableFrom(fieldType)) {
                    notBlank((String) fieldValue, fieldName);
                } else {
                    notNull(fieldValue, fieldName);
                }
            } catch (IllegalAccessException e) {
                throw new RuntimeException("Field '" + fieldType.getSimpleName() + "." + fieldName + "' is not accessible: " + e.getMessage(), e);
            } catch (SecurityException e) {
                throw new RuntimeException("Field '" + fieldType.getSimpleName() + "." + fieldName + "' is not accessible: " + e.getMessage(), e);
            }
        }
    }

}
