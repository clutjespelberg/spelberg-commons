package nl.spelberg.commons.utils.dao;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public abstract class AbstractQueryEntity {
    /**
     * Implementatie van {@link Object#equals(Object) equals(Object o)} met {@link org.apache.commons.lang3.builder
     * .EqualsBuilder#reflectionEquals(Object,
     * Object) EqualsBuilder.reflectionEquals(this, o)}
     */
    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    public boolean equalsId(Object obj) {
        // TODO: maak hier een abstract method van die geimplementeerd moet worden
        throw new UnsupportedOperationException("Implement this method in the subclass: " + getClass().getName());
    }

    /**
     * Implementatie van {@link Object#hashCode() hashCode()} met {@link org.apache.commons.lang3.builder
     * .HashCodeBuilder#reflectionHashCode(Object)
     * HashCodeBuilder.reflectionHashCode(this)} <br/>
     * <br/>
     * Override {@link #hashCodeOverridden()} om een eigen implementatie te gebruiken.
     */
    @Override
    public final int hashCode() {
        return hashCodeOverridden();
    }

    protected int hashCodeOverridden() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    /**
     * Implementatie van {@link Object#toString() toString()} met {@link org.apache.commons.lang3.builder
     * .ReflectionToStringBuilder#toString(Object,
     * org.apache.commons.lang3.builder.ToStringStyle) ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE)}
     * <br/>
     * <br/>
     * Override {@link #toStringOverridden()} om een eigen implementatie te gebruiken.
     */
    @Override
    public final String toString() {
        return toStringOverridden();
    }

    protected String toStringOverridden() {
        return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
