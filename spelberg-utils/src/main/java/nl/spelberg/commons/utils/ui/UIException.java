package nl.spelberg.commons.utils.ui;

public class UIException extends RuntimeException {

    public static enum Severity {
        WARNING,
        ERROR
    }

    public static final Severity DEFAULT_LEVEL = Severity.ERROR;
    public static final String DEFAULT_TITLE = "Onverwachte fout";

    private String title;
    private Severity severity;

    public UIException(Severity severity, String userMessage) {
        this(severity, DEFAULT_TITLE, userMessage);
    }

    public UIException(Severity severity, String title, String userMessage) {
        super(userMessage);
        this.title = title == null ? DEFAULT_TITLE : title;
        this.severity = severity;
    }

    public boolean hasErrorSeverity() {
        return Severity.ERROR.equals(severity);
    }

    public String getTitle() {
        return title;
    }

    public Severity getSeverity() {
        return severity;
    }
}
