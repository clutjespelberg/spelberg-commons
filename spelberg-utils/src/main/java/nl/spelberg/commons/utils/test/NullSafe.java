package nl.spelberg.commons.utils.test;

import org.springframework.util.Assert;

public class NullSafe {

    /**
     * Returns een null-safe object, nooit null.
     *
     * @param object Een object dat niet null zou moeten zijn.
     * @return Het object, nooit null.
     * @throws IllegalArgumentException als het object null is.
     * @see #value(Object, String)
     * @see Assert#notNull(Object)
     */
    public static <T> T value(T object) {
        return value(object, "value is null");
    }

    /**
     * Returns een null-safe object, nooit null.
     *
     * @param object  Een object dat niet null zou moeten zijn.
     * @param message Het bericht dat in de exception gezet moet worden als object null is.
     * @return Het object, nooit null.
     * @throws IllegalArgumentException als het object null is.
     * @see #value(Object)
     * @see Assert#notNull(Object, String)
     */
    public static <T> T value(T object, String message) {
        Assert.notNull(object, message);
        return object;
    }

    /**
     * Null-safe compareTo waarbij null altijd kleiner is dan een niet-null.
     */
    public static <T extends Comparable<? super T>> int compareTo(T left, T right) {
        int result = 0;
        if (left == null) {
            result = right == null ? 0 : -1;
        } else {
            // left is not null
            if (right == null) {
                result = 1;
            } else {
                result = left.compareTo(right);
            }
        }
        return result;
    }

}
