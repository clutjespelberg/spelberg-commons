package nl.spelberg.commons.utils.validation;

import nl.spelberg.commons.utils.ui.UIException;

public class JsonValidationException extends UIException {

    public JsonValidationException(String userMessage) {
        super(Severity.WARNING, userMessage);
    }

    public JsonValidationException(String title, String userMessage) {
        super(Severity.WARNING, title, userMessage);
    }

    public JsonValidationException(String userMessage, JsonValidationException cause) {
        super(Severity.WARNING, cause == null ? userMessage : userMessage + " - " + cause.getMessage());
    }

    public JsonValidationException(JsonValidationException e) {
        this(e.getTitle(), e.getMessage());
    }
}
