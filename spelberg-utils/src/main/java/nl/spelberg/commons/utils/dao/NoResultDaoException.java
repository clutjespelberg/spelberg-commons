package nl.spelberg.commons.utils.dao;

import javax.persistence.NoResultException;
import javax.persistence.Query;

public class NoResultDaoException extends NoResultException {

    public NoResultDaoException(Query query, NoResultException cause) {
        super("No result: " + DaoUtils.queryToString(query) + ", cause: " + cause.getMessage());
        if (cause instanceof NoResultDaoException) {
            throw new IllegalArgumentException("cause instanceof NoResultDaoException", cause);
        }
        initCause(cause);
    }

}
