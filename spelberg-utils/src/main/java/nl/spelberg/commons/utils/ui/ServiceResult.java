package nl.spelberg.commons.utils.ui;

import javax.xml.bind.annotation.XmlRootElement;

@SuppressWarnings({"UnusedDeclaration"})
@XmlRootElement
public class ServiceResult extends AbstractJsonObject {
    public enum Status {SUCCES, FOUT, ERROR}
    private String message;
    private Status status;

    @Deprecated
    protected ServiceResult() {
        this.message = "";
        this.status = Status.SUCCES;
    }

    public ServiceResult(Status status) {
        this.status = status;
        this.message = status.name();
    }

    public ServiceResult(Status status, String message) {
        this.status = status;
        this.message = message;
    }

}
