package nl.spelberg.commons.utils.ui;

import java.util.Random;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Provider
public class GeneralExceptionMapper implements ExceptionMapper<Throwable> {

    private static final Logger log = LoggerFactory.getLogger(GeneralExceptionMapper.class);

    public static final Random RANDOM = new Random();

    @Override
    public final Response toResponse(Throwable exception) {
        Response.ResponseBuilder builder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
        builder.type(javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE);
        builder.entity(createJsonError(exception));
        return builder.build();
    }

    protected JsonError createJsonError(Throwable throwable) {

        String meldingsCode = Integer.toHexString(RANDOM.nextInt());
        String meldingsCodeMessage = "Meldingscode: " + meldingsCode;
        String logMessage = meldingsCodeMessage + " Cause: " + throwable.getMessage();

        if (throwable instanceof UIException) {

            UIException uiException = (UIException) throwable;
            if (uiException.hasErrorSeverity()) {
                log.error(logMessage, uiException);
            } else {
                log.debug(logMessage, uiException);
            }
            return new JsonError(uiException);

        } else {

            log.error(logMessage, throwable);

            String userMessage = "Er is iets fout gegaan. " + meldingsCodeMessage;
            return new JsonError(UIException.DEFAULT_LEVEL, UIException.DEFAULT_TITLE, userMessage);

        }
    }
}
