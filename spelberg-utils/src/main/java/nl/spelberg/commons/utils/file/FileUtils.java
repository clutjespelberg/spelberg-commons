package nl.spelberg.commons.utils.file;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.nio.charset.UnsupportedCharsetException;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

public class FileUtils {

    public static File createTempFileFromClasspath(String prefix, String suffix, InputStream in) {

        try {
            File tempFile = File.createTempFile(prefix, suffix);
            tempFile.deleteOnExit();

            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            OutputStream out = new FileOutputStream(tempFile);
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(out));

            String line;
            while ((line = br.readLine()) != null) {
                bw.write(line);
                bw.newLine();
            }
            bw.close();
            br.close();

            return tempFile;
        } catch (IOException e) {
            throw new RuntimeException("Error creating tempfile. prefix='" + prefix + "', suffix='" + suffix + "', message: "  + e.getMessage(), e);
        }


    }

    public static String pathFromClass(Class<?> aClass) {
        return aClass == null ? null : StringUtils.replace(aClass.getName(), ".", "/");
    }

    public static String pathFromObject(Object o) {
        return o == null ? null : pathFromClass(o.getClass());
    }

    public static String asUtf8String(byte[] bytes) {
        return new String(bytes, charSetUTF8());
    }

    public static byte[] asUtf8Bytes(String string) {
        return string.getBytes(charSetUTF8());
    }

    public static Charset charSetUTF8() {
        Charset charSet;
        try {
            charSet = Charset.forName("UTF-8");
        } catch (UnsupportedCharsetException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
        return charSet;
    }

    /**
     * @return het pad naar de temp directory met systeemafhankelijke separators (/ of \ ) en met separator aan het eind
     */
    public static String getTempDir() {
        String tempDir = System.getProperty("java.io.tmpdir");
        return FilenameUtils.normalizeNoEndSeparator(tempDir) + File.separator;
    }
}