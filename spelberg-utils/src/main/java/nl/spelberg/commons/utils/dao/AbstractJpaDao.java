package nl.spelberg.commons.utils.dao;

import java.lang.reflect.ParameterizedType;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Pieter Bruining <pieter.bruining@spelberg.nl>
 */
@SuppressWarnings({"JpaQlInspection"})
@Transactional(propagation = Propagation.MANDATORY)
public abstract class AbstractJpaDao<ENTITY, ID> implements JpaDaoInterface<ENTITY, ID> {

    private final static Logger log = LoggerFactory.getLogger(AbstractJpaDao.class);
    protected Class<ENTITY> entityClass;

    protected EntityManager entityManager;

    @PersistenceContext
    public void setEntityManager(EntityManager realEntityManager) {
        this.entityManager = new DaoEntityManager(realEntityManager);
    }

    @SuppressWarnings({"unchecked"})
    public AbstractJpaDao() {
        try {
            ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
            this.entityClass = (Class<ENTITY>) genericSuperclass.getActualTypeArguments()[0];
        } catch (RuntimeException e) {
            log.warn("Could not detect entity class");
            throw e;
        }
    }

    @Override
    public final ENTITY find(ID id) {
        return this.entityManager.find(entityClass, id);
    }

    @Override
    public final ENTITY get(ID id) {
        ENTITY result = this.find(id);
        if (result == null) {
            throw new NoResultException("Could not find " + this.entityClass.getSimpleName() + " with id " + id);
        } else {
            return result;
        }
    }

    @Override
    public final List<ENTITY> findAll() {
        TypedQuery<ENTITY> query = this.entityManager.createQuery("SELECT o FROM " + this.entityClass.getSimpleName() + " o", this.entityClass);
        return query.getResultList();
    }

    @Override
    public final ENTITY persist(ENTITY entity) {
        this.entityManager.persist(entity);
        //
        // call flush because we want to fail fast when the entity already exists. From the EntityManager.persist(entity) javadoc:
        //
        // "If the entity already exists, the EntityExistsException may be thrown when the persist operation is invoked,
        // or the EntityExistsException or another PersistenceException may be thrown at flush or commit time."
        //
        this.entityManager.flush();
        return entity;
    }

    @Override
    public final void persistMany(Iterable<ENTITY> entities) {
        for (ENTITY entity : entities) {
            this.entityManager.persist(entity);
        }
        //
        // call flush because we want to fail fast when the entity already exists. From the EntityManager.persist(entity) javadoc:
        //
        // "If the entity already exists, the EntityExistsException may be thrown when the persist operation is invoked,
        // or the EntityExistsException or another PersistenceException may be thrown at flush or commit time."
        //
        this.entityManager.flush();
    }

    @Override
    public final void delete(ENTITY entity) {
        this.entityManager.remove(entity);
        //
        // call flush because we want queries in the same transaction to reflect the removal of the entity
        //
        this.entityManager.flush();
    }

    @Override
    public final void deleteById(ID id) {
        ENTITY entity = get(id);
        delete(entity);
    }

    /**
     * TODO: geen idee waarom flush nodig is, nog uitzoeken en dan weer netjes weghalen
     */
    @Override
    @Deprecated
    public final void flush() {
        entityManager.flush();
    }

    @Override
    public void deleteMany(List<ENTITY> entities) {
        for (ENTITY entity : entities) {
            this.entityManager.remove(entity);
        }
        //
        // call flush because we want queries in the same transaction to reflect the removal of the entity
        //
        this.entityManager.flush();
    }
}
