package nl.spelberg.commons.utils.test;

public class EqualsUtils {

    public static <T> boolean equalsWithNull(T o1, T o2) {
        return o1 == null ? o2 == null : o1.equals(o2);
    }

}
