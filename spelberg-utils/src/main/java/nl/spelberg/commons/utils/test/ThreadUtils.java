package nl.spelberg.commons.utils.test;

public class ThreadUtils {

    /**
     * Wrapper for {@link Thread#sleep(long)} to avoid the try .. catch {@link InterruptedException}.
     *
     * @param millis the length of time to sleep in milliseconds.
     * @see Thread#sleep(long)
     */
    public static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    /**
     * Returns the object and retries every 100ms.
     * @param maxWaitMillis max time to wait in milliseconds
     * @param waitCondition the wait condition
     * @return the object returned by the wait condition, never null
     * @throws AssertionError if a timeout occurred
     */
    public static <T> T waitForCondition(long maxWaitMillis, WaitCondition<T> waitCondition) {
        long startMillis = System.currentTimeMillis();
        while (System.currentTimeMillis() - startMillis < maxWaitMillis) {
            T validResult = waitCondition.getValidResult();
            if (validResult != null) {
                return validResult;
            } else {
                sleep(100);
            }
        }
        throw new AssertionError("FAIL: Wait for condition timeout after " + maxWaitMillis + " millis: " + waitCondition.getFailMessage());
    }

    public static abstract class WaitCondition<T> {
        private final String failMessage;

        public WaitCondition(String failMessage) {
            this.failMessage = failMessage;
        }

        public final String getFailMessage() {
            return failMessage;
        }

        /**
         * Check condition.
         * @return null value if condition is not met, data object if the condition is met.
         */
        public abstract T getValidResult();
    }
}
