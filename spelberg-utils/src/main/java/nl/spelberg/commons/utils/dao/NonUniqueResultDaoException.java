package nl.spelberg.commons.utils.dao;

import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;

public class NonUniqueResultDaoException extends NonUniqueResultException {

    public NonUniqueResultDaoException(Query query, NonUniqueResultException cause) {
        super("Non unique result: " + DaoUtils.queryToString(query) + ", cause: " + cause.getMessage());
        if (cause instanceof NonUniqueResultDaoException) {
            throw new IllegalArgumentException("cause instanceof NonUniqueResultDaoException", cause);
        }
        initCause(cause);
    }

}
