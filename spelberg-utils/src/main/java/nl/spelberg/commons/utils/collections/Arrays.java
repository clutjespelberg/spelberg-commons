package nl.spelberg.commons.utils.collections;

import java.util.List;
import org.apache.commons.lang3.ArrayUtils;

public class Arrays {

    /**
     * Calls {@link java.util.Arrays#asList(Object[])} directly.
     */
    @SuppressWarnings({"unchecked"})
    public static <T> List<T> asList(T... elements) {
        return java.util.Arrays.asList(elements);
    }

    /**
     * Calls {@link java.util.Arrays#toString()} directly.
     */
    public static <T> String toString(T[] array) {
        return java.util.Arrays.toString(array);
    }

    @SuppressWarnings({"unchecked"})
    public static <T> T[] join(T head, T[] tail) {
        return ArrayUtils.add(tail, 0, head);
    }

    @SuppressWarnings({"unchecked"})
    public static <T> T[] join(T[] array1, T... array2) {
        return ArrayUtils.addAll(array1, array2);
    }
}
