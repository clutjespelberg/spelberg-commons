package nl.spelberg.commons.utils.collections;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import nl.spelberg.commons.utils.dao.AbstractQueryEntity;
import org.springframework.util.Assert;

public class JpaCollectionUtils {

    /**
     * Returns a list containing the unique entities from the source list, based on {@link AbstractQueryEntity#equalsId(Object)}.
     * The order of the list is retained, for duplicate entities the first one is kept.<br/>
     * <br/>
     * Note: this method only compares using {@link AbstractQueryEntity#equalsId(Object)}, it does not use
     * {@link Object#equals(Object)} or {@link Object#hashCode()}.
     *
     * @param list The source list, will not be modified
     * @return A new list containing the unique entities
     */
    public static <E extends AbstractQueryEntity> List<E> asUniqueIdList(List<E> list) {
        Assert.notNull(list, "list is null");
        ArrayList<E> resultList = new ArrayList<E>();
        for (E entity : list) {
            boolean exists = false;
            for (E resultEntity : resultList) {
                if (entity.equalsId(resultEntity)) {
                    exists = true;
                    break;
                }
            }
            if (!exists) {
                resultList.add(entity);
            }
        }
        return resultList;
    }

    /**
     * Update een JPA entity list met nieuwe waarden, waarbij de niet-voorkomende waarden worden verwijderd uit de
     * jpaEntityList en nieuwe waarden worden toegevoegd op de <b>positie</b> die ze hadden in newValuesList,
     * reeds bestaande waarden blijven bestaan, in de <b>volgorde</b> die ze voorheen al hadden
     *
     * @param jpaEntityList De door JPA gemanagde list (meestal met @OneToMany annotatie)
     * @param newValuesList De list met de nieuwe waarden.
     */
    public static <ENTITY, VALUE> void updateJpaList(List<ENTITY> jpaEntityList, List<VALUE> newValuesList,
            UpdateFactory<ENTITY, VALUE> factory) {
        Assert.notNull(jpaEntityList, "jpaEntityList is null");
        Assert.notNull(newValuesList, "newValuesList is null");
        Assert.notNull(factory, "factory is null");

        List<VALUE> toAddValues = new ArrayList<VALUE>(newValuesList);

        Iterator<ENTITY> propertyValuesIterator = jpaEntityList.iterator();
        while (propertyValuesIterator.hasNext()) {
            ENTITY existingValue = propertyValuesIterator.next();

            boolean newValueExists = false;
            Iterator<VALUE> toAddIterator = toAddValues.iterator();
            while (toAddIterator.hasNext()) {
                VALUE newValue = toAddIterator.next();
                if (factory.equalsExistingValue(existingValue, newValue)) {
                    toAddIterator.remove();
                    newValueExists = true;
                    break;
                }
            }
            if (!newValueExists) {
                propertyValuesIterator.remove();
            }
        }

        for (VALUE toAddValue : toAddValues) {
            ENTITY newExistingValue = factory.createExistingValue(toAddValue);

            jpaEntityList.add(newValuesList.indexOf(toAddValue), newExistingValue);
        }

    }

    public static interface UpdateFactory<ENTITY, VALUE> {

        boolean equalsExistingValue(ENTITY existingValue, VALUE newValue);

        ENTITY createExistingValue(VALUE newValue);

    }

}
