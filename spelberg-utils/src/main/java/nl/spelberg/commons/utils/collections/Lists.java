package nl.spelberg.commons.utils.collections;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;
import org.springframework.core.convert.converter.Converter;
import org.springframework.util.Assert;

public class Lists {

    public static <S, T> List<T> convert(S[] source, Converter<S, T> converter) {
        ArrayList<T> list = new ArrayList<T>();
        for (S s : source) {
            list.add(converter.convert(s));
        }
        return list;
    }

    public static <S, T> List<T> convert(Iterable<S> source, Converter<S, T> converter) {
        ArrayList<T> list = new ArrayList<T>();
        for (S s : source) {
            list.add(converter.convert(s));
        }
        return list;
    }

    public static <T> List<T> prepend(T head, Collection<T> tail) {
        ArrayList<T> list = new ArrayList<T>();
        list.add(head);
        list.addAll(tail);
        return list;
    }

    /**
     * Converteer een array waarin <tt>null</tt> elementen kunnen voorkomen naar een
     * lijst waaruit alle <tt>null</tt> elementen zijn gefilterd
     */
    public static <T> List<T> toNullFilteredList(T[] elements) {
        List<T> result = new ArrayList<T>();
        for (T element : elements) {
            if (element != null) {
                result.add(element);
            }
        }
        return result;
    }

    public static <T> List<T> reverse(List<T> list) {
        ArrayList<T> reversed = new ArrayList<T>(list.size());
        for (int i = list.size() - 1; i >= 0; i--) {
            T value = list.get(i);
            reversed.add(value);
        }
        return reversed;
    }

    public static <T extends Comparable<? super T>> List<T> mergeSorted(Collection<T> list1, Collection<T> list2) {
        List<T> merged = new ArrayList<T>();
        merged.addAll(list1);
        merged.addAll(list2);
        Collections.sort(merged);
        return merged;
    }

    public static <T> List<T> mergeSorted(List<T> list1, List<T> list2, Comparator<? super T> comparator) {
        List<T> merged = new ArrayList<T>();
        merged.addAll(list1);
        merged.addAll(list2);
        Collections.sort(merged, comparator);
        return merged;
    }

    /**
     * Copied from {@link org.apache.commons.collections.ListUtils#subtract(java.util.List, java.util.List)} and generified.<br/>
     * <br/>
     * Subtracts all elements in the second list from the first list,
     * placing the results in a new list.
     * <p>
     * This differs from {@link List#removeAll(Collection)} in that
     * cardinality is respected; if <Code>list1</Code> contains two
     * occurrences of <Code>null</Code> and <Code>list2</Code> only
     * contains one occurrence, then the returned list will still contain
     * one occurrence.
     *
     * @param originalValues the list to subtract from
     * @param subtractValues the list to subtract
     * @return a new list containing the results
     */
    public static <T> List<T> subtract(List<T> originalValues, List<T> subtractValues) {
        Assert.notNull(originalValues, "originalValues is null");
        Assert.notNull(subtractValues, "subtractValues is null");

        final List<T> result = new ArrayList<T>(originalValues);
        for (T value : subtractValues) {
            result.remove(value);
        }
        return result;
    }

    /**
     * Copied from {@link java.util.AbstractList#equals(Object)} to be usable for any list. Compares 2 lists by iterating
     * over both lists.<br/>
     * <br/>
     * Neede in case a Hibernate list has to be compared (implemented by a {@link org.hibernate.collection.PersistentBag})
     */
    public static <T> boolean equals(List<T> l1, List<T> l2) {
        if (l1 == l2) {
            return true;
        }

        ListIterator<T> it1 = l1.listIterator();
        ListIterator<T> it2 = l2.listIterator();
        while (it1.hasNext() && it2.hasNext()) {
            T value1 = it1.next();
            T value2 = it2.next();
            if (!(value1 == null ? value2 == null : value1.equals(value2))) {
                return false;
            }
        }
        return !(it1.hasNext() || it2.hasNext());

    }

    /**
     * Convenience method that uses {@link Arrays#asList(Object[])}.
     */
    public static <T> List<T> asList(T... values) {
        Assert.notNull(values, "values is null");
        return Arrays.asList(values);
    }

    /**
     * Filtert dubbele elementen uit een list.
     */
    public static <T> List<T> asUniqueList(List<T> list) {
        Set<T> set = Sets.asUniqueSet(list);
        return new ArrayList<T>(set);
    }

    public static <T> List<T> join(List<T>... lists) {
        ArrayList<T> result = new ArrayList<T>();
        for (List<T> list : lists) {
            result.addAll(list);
        }
        return result;
    }

    public static <T> T lastElement(List<T> list) {
        return list.isEmpty() ? null : list.get(list.size() - 1);
    }

}
