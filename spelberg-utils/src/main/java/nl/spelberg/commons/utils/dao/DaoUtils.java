package nl.spelberg.commons.utils.dao;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Parameter;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.Assert;

public class DaoUtils {

    public static String queryToString(Query query) {
        Assert.notNull(query, "query is null");
        String simpleName = query.getClass().getSimpleName();
        return simpleName + "(parameters: " + parametersToString(query) + ", query: " + queryToQueryString(query) + ")";
    }

    public static String parametersToString(Query query) {
        Assert.notNull(query, "query is null");
        Set<Parameter<?>> parameters = query.getParameters();
        if (parameters.isEmpty()) {
            return "<none>";
        } else {
            List<String> parameterToStringList = new ArrayList<String>();
            for (Parameter<?> parameter : parameters) {
                String parameterName = parameter.getName();

                String parameterToString;
                try {
                    if (query.isBound(parameter)) {
                        Object value = query.getParameterValue(parameterName);
                        if (value == null) {
                            parameterToString = parameterName + " is null";
                        } else {
                            if (value instanceof Number) {
                                parameterToString = parameterName + "=" + value;
                            } else {
                                parameterToString = parameterName + "='" + value + "'";
                            }
                        }
                    } else {
                        parameterToString = parameterName + " is not bound";
                    }
                } catch (Exception e) {
                    parameterToString = parameterName + " exception: '" + e.getMessage() + "'";
                }
                parameterToStringList.add(parameterToString);
            }
            return StringUtils.join(parameterToStringList, ",");
        }
    }

    public static String queryToQueryString(Query query) {
        Object value = getFieldValue(query, "query", "queryString");
        return value == null ? null : value.toString();
    }

    public static Object getFieldValue(Object instance, String... fieldNames) {
        for (String fieldName : fieldNames) {
            instance = getFieldValue(instance, fieldName);
            if (instance == null) {
                return null;
            }
        }
        return instance;
    }

    public static Object getFieldValue(Object instance, String fieldName) {
        Assert.notNull(fieldName, "fieldName is null");
        if (instance == null) {
            return null;
        }
        try {
            Class<?> instanceClass = instance.getClass();
            Field field = getDeclaredField(fieldName, instanceClass);
            field.setAccessible(true);
            return field.get(instance);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e.getMessage(), e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    private static Field getDeclaredField(String fieldName, Class<?> instanceClass) throws NoSuchFieldException {
        try {
            return instanceClass.getDeclaredField(fieldName);
        } catch (NoSuchFieldException e) {
            Class<?> superClass = instanceClass.getSuperclass();
            if (superClass == null) {
                throw e;
            } else {
                return getDeclaredField(fieldName, superClass);
            }
        }
    }

    @SuppressWarnings("unchecked")
    public static <Q extends Query> Q createQueryProxy(final Q query) {
        return (Q) Proxy.newProxyInstance(AbstractJpaDao.class.getClassLoader(), new Class[]{TypedQuery.class}, new QueryInvocationHandler(query));
    }

    private static class QueryInvocationHandler implements InvocationHandler {

        private final Query query;

        public QueryInvocationHandler(Query query) {
            Assert.notNull(query, "query is null");
            this.query = query;
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            try {

                if (args != null && args.length > 1 && method.getName().equals("setParameter")) {
                    // voorkomen van fouten doordat null als parameter value wordt meegegeven
                    Assert.notNull(args[1], "setParameter: value is null");
                }

                // normal method invocation
                try {
                    return method.invoke(query, args);
                } catch (InvocationTargetException e) {
                    // als er een exception optreedt dan wordt deze ingepakt in een InvocationTargetException
                    throw e.getCause();
                }

            } catch (NoResultException e) {
                throw new NoResultDaoException(query, e);
            } catch (NonUniqueResultException e) {
                throw new NonUniqueResultDaoException(query, e);
            }
        }
    }

}
