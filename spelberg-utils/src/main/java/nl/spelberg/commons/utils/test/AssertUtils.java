package nl.spelberg.commons.utils.test;

import org.apache.commons.lang3.StringUtils;

public class AssertUtils {

    public static void assertException(Runnable failingRunnable, Class<? extends Throwable> exceptionClass,
            String expectedMessage) {
        String expectedExceptionWithMessage = formatException(exceptionClass, expectedMessage);
        try {
            failingRunnable.run();
            throw new AssertionError("Expected " + expectedExceptionWithMessage + " but no exception was thrown.");
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable t) {
            assertEqualsNoJUnit(expectedExceptionWithMessage, formatException(t));
        }
    }

    public static String formatException(Throwable t) {
        if (t == null) {
            return "<null>";
        } else {
            return formatException(t.getClass(), t.getMessage());
        }
    }

    public static String formatException(Class<? extends Throwable> throwableClass, String message) {
        return throwableClass == null ? "<null>" : throwableClass.getName() + "(\"" + message + "\")";
    }

    public static <T> void assertEqualsNoJUnit(T expected, T actual) {
        if (notEqual(expected, actual)) {
            throw new AssertionError("Objects not equal.\nExpected: " + expected + "\n  actual: " + actual);
        }
    }

    public static <T> void stateEqual(T expected, T actual, String variableName) {
        if (notEqual(expected, actual)) {
            throw new IllegalStateException("Variable '" + variableName + "' not as expected.\nExpected: " + expected + "\n  actual: " + actual);
        }
    }

    public static <T> boolean notEqual(T o1, T o2) {
        return !objectsEqual(o1, o2);
    }

    public static <T> boolean objectsEqual(T o1, T o2) {
        return o1 == null ? o2 == null : o1.equals(o2);
    }

    public static void stateNotNull(Object value, String fieldName) {
        if (value == null) {
            throw new IllegalStateException("'" + fieldName + "' is null");
        }
    }

    public static void stateHasText(String value, String fieldName) {
        if (StringUtils.isBlank(value)) {
            throw new IllegalStateException("'" + fieldName + "' heeft geen waarde: '" + value + "'");
        }
    }
}
