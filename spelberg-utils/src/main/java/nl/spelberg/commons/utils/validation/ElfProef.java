package nl.spelberg.commons.utils.validation;

import java.util.Arrays;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

public class ElfProef {

    private static final Logger log = LoggerFactory.getLogger(ElfProef.class);

    private static final int FACTOR = 11;

    public static final ElfProef BANKREKENING_9 = new ElfProef("BANKREKENING_9", 9, 8, 7, 6, 5, 4, 3, 2, 1);
    public static final ElfProef BANKREKENING_10 = new ElfProef("BANKREKENING_10", 10, 9, 8, 7, 6, 5, 4, 3, 2, 1);
    public static final ElfProef BANKREKENING_10_INVERSE = new ElfProef("BANKREKENING_10_INVERSE", 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
    public static final ElfProef BSN = new ElfProef("BSN", 9, 8, 7, 6, 5, 4, 3, 2, -1);

    private final String type;
    private final int[] factors;

    public ElfProef(String type, int... factors) {
        Assert.hasText(type, "type bevat geen text");
        Assert.notNull(factors, "factors is null");
        Assert.isTrue(factors.length > 0, "factors is empty");
        this.type = type;
        this.factors = factors;
    }

    public boolean isValid(int... numbers) {
        Assert.notNull(numbers, "numbers is null");
        Assert.isTrue(numbers.length <= factors.length, "numbers.length > factors.length");

        if (numbers.length == 0){
            return false;
        }

        int sum = 0;
        for (int i = 0; i < numbers.length; i++) {
            int number = numbers[i];
            int factor = factors[i];
            sum += factor * number;
        }

        int modulusFactor = sum % FACTOR;
        boolean valid = modulusFactor == 0;
        if (valid) {
            return true;
        } else {
            if (log.isDebugEnabled()) {
                log.debug(getClass().getSimpleName() + ": " + toString(numbers));
            }
            return false;
        }
    }

    public boolean isValid(String numbersValue) {
        int length = numbersValue.length();



        int[] numbers = new int[length];

        char[] charArray = numbersValue.toCharArray();
        for (int i = 0, charArrayLength = charArray.length; i < charArrayLength; i++) {
            char c = charArray[i];
            numbers[i] = Integer.parseInt(String.valueOf(c));
        }

        return isValid(numbers);
    }

    public void assertValid(int... numbers) {
        if (!isValid(numbers)) {
            throw new IllegalArgumentException("ongeldige " + type + ": '" + toString(numbers) + "'");
        }
    }

    public void assertValid(String numbersValue) {
        if (!isValid(numbersValue)) {
            throw new IllegalArgumentException("ongeldige " + type + ": '" + numbersValue + "'");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ElfProef)) {
            return false;
        }

        ElfProef elfProef = (ElfProef) o;

        if (!Arrays.equals(factors, elfProef.factors)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return factors != null ? Arrays.hashCode(factors) : 0;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

    private String toString(int[] intArray) {
        StringBuilder sb = new StringBuilder();
        for (int number : intArray) {
            sb.append(number);
        }
        return sb.toString();
    }

    public String nextValid(String valueText) {
        long value = Long.parseLong(valueText);
        String s;
        do {
            s = Long.toString(value);
            value++;
        } while (!isValid(s) && s.length() == valueText.length());
        return s;
    }
}
