package nl.spelberg.commons.utils.spring;

import nl.spelberg.commons.utils.validation.JsonValidationException;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ErrorHandlingInterceptor implements MethodInterceptor {

    private static final Logger log = LoggerFactory.getLogger(ErrorHandlingInterceptor.class);

    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
        try {

            return invocation.proceed();

        } catch (JsonValidationException e) {
            // validationexceptions worden gebruikt voor foutafhandeling dus die moeten doorgegooid worden
            throw createAndLogValidationException(e);

        } catch (RuntimeException e) {
            // unchecked exceptions worden niet verwacht en moeten netjes afgehandeld worden
            throw createAndLogErrorHandlingInterceptorException(e);

        } catch (Exception e) {
            // checked exceptions worden verwacht dus die kunnen gewoon doorgegooid worden
            throw e;

        } catch (Throwable t) {
            // alle overige exceptions en errors worden niet verwacht en moeten netjes afgehandeld worden
            throw createAndLogErrorHandlingInterceptorException(t);
        }
    }

    private ErrorHandlingInterceptorException createAndLogErrorHandlingInterceptorException(Throwable t) {
        String message = "Remote Invocation Error: " + t.getClass().getName() + "('" + t.getMessage() + "')";
        log.error(message, t);
        return new ErrorHandlingInterceptorException(message);
    }

    private JsonValidationException createAndLogValidationException(JsonValidationException e) {
        log.debug(e.getMessage(), e);
        return new JsonValidationException(e);
    }

}
