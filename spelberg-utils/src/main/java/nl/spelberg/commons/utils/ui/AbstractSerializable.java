package nl.spelberg.commons.utils.ui;

import java.io.Serializable;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Gebruik deze class als basis voor domein value objecten.
 * <ul>
 * <li>implements {@link java.io.Serializable}</li>
 * <li>{@link Object#equals(Object) equals(Object o)} met {@link EqualsBuilder#reflectionEquals(Object, Object, String...)
 * EqualsBuilder.reflectionEquals(this, o)}</li>
 * <li>{@link Object#hashCode() hashCode()} met {@link HashCodeBuilder#reflectionHashCode(Object, String...)
 * HashCodeBuilder.reflectionHashCode(this)}</li>
 * <li>{@link Object#toString() toString()} met {@link ReflectionToStringBuilder#toString(Object,
 * org.apache.commons.lang3.builder.ToStringStyle) ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE)}</li>
 * </ul>
 */
public abstract class AbstractSerializable implements Serializable {

    /**
     * Implementatie van {@link Object#equals(Object) equals(Object o)} met {@link EqualsBuilder#reflectionEquals(Object, Object, String...) EqualsBuilder.reflectionEquals(this, o)}
     */
    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    /**
     * Implementatie van {@link Object#hashCode() hashCode()} met {@link HashCodeBuilder#reflectionHashCode(Object, String...)
     * HashCodeBuilder.reflectionHashCode(this)} <br/>
     * <br/>
     * Override {@link #hashCodeOverridden()} om een eigen implementatie te gebruiken.
     */
    @Override
    public final int hashCode() {
        return hashCodeOverridden();
    }

    protected int hashCodeOverridden() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    /**
     * Implementatie van {@link Object#toString() toString()} met {@link ReflectionToStringBuilder#toString(Object,
     * org.apache.commons.lang3.builder.ToStringStyle) ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE)}
     * <br/>
     * <br/>
     * Override {@link #toStringOverridden()} om een eigen implementatie te gebruiken.
     */
    @Override
    public final String toString() {
        return toStringOverridden();
    }

    protected String toStringOverridden() {
        return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
